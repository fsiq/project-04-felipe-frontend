import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from '@reach/router'

import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import Divider from '@material-ui/core/Divider'
import HomeIcon from '@material-ui/icons/Home'
import SettingsIcon from '@material-ui/icons/Settings'

import ImageAvatars from '~/components/layout/admin/avatar'
import { Button, MenuHeader, UserEmailHeader, UserNameHeader, STypography, LogoutButton, MenuItemBuntton } from '~/components/layout/admin/avatar-menu/styled'
import { logoutAction } from '~/store/auth/auth.action'
export default function SimpleMenu({ sideBar }) {
  const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = React.useState(null)

  const user = useSelector(state => state.auth.usuario)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const logout = () => {
    dispatch(logoutAction())
  }

  return (
    <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <ImageAvatars />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuList>
          <MenuHeader>
            <UserNameHeader>{user.name}</UserNameHeader>
            <UserEmailHeader>{user.email}</UserEmailHeader>
          </MenuHeader>

          <Divider />

          <MenuItem>
            <HomeIcon fontSize="small" color="inherit" />
            <STypography><Link to="/">Home</Link></STypography>
          </MenuItem>
          {/* <MenuItem>
            <PersonIcon fontSize="small" color="inherit" />
            <STypography>Profile</STypography>
          </MenuItem> */}
          <MenuItem>
            <SettingsIcon fontSize="small" color="inherit" />
            <STypography><Link to={user.kindUser === 1 ? '/admin/settings' : '' || user.kindUser === 2 ? '/partner/settings' : ''}>Settings</Link></STypography>
          </MenuItem>
          <MenuItemBuntton>
            <LogoutButton onClick={logout}>
              Logout
            </LogoutButton>
          </MenuItemBuntton>
        </MenuList>
      </Menu>
    </div>
  )
}
