import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import defaultImg from '~/assets/images/fallback-images/defaultUser.png'

const AvatarContainer = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-shrink: 0;
    width: 50px;
    height: 50px;
    font-family: "Public Sans", sans-serif;
    font-size: 1.25rem;
    line-height: 1;
    border-radius: 50%;
    overflow: hidden;
    user-select: none;
`

const ImgProfile = styled.img`
    width: 100%;
    height: 100%;
    text-align: center;
    object-fit: cover;
    color: transparent;
    text-indent: 10000px;
`

export default function ImageAvatars() {
  const user = useSelector(state => state.auth.usuario)

  const ValidateUserImage = (user.image === '/static/profile/undefined') ? undefined : user.image

  const image = process.env.REACT_APP_STATIC + user.image

  const avatarImage = (ValidateUserImage !== undefined && ValidateUserImage !== null) ? image : defaultImg

  return (
    <AvatarContainer>
      <ImgProfile alt="imagem de perfil" src={avatarImage} />
    </AvatarContainer>
  )
}
