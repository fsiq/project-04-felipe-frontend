import React from 'react'
import { useSelector } from 'react-redux'
import {
  CardContainer,
  SCard,
  BoxText,
  Stitle,
  SsubTittle
} from '~/components/layout/admin/cards/styled'
import ImageAvatars from '~/components/layout/admin/avatar'

export default function AdminPerfilCard() {
  const user = useSelector(state => state.auth.usuario)

  let kindString = ''

  switch (user.kindUser) {
    case 1:
      kindString = 'Admin'
      break

    case 2:
      kindString = 'Partern'
      break
    case 3:
      kindString = 'Client'
      break
    default:
      break
  }

  return (
    <CardContainer>
      <SCard>
        <ImageAvatars />
        <BoxText>
          <Stitle>{user.name}</Stitle>
          <SsubTittle>{kindString}</SsubTittle>
        </BoxText>
      </SCard>
    </CardContainer>
  )
}
