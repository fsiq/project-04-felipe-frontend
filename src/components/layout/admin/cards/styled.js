import styled from 'styled-components'
import Card from '@material-ui/core/Card'

const CardContainer = styled.div`
    margin-bottom: 16px;
`
const SCard = styled(Card)`
    display: flex;
    align-items: center;
    padding: 10px 10px;
    margin-bottom: 30px;
    border-radius: 12px;
    background-color: rgba(145, 158, 171, 0.1);
`
const BoxText = styled.div`
  margin-left: 16px;
`
const Stitle = styled.h6`
    margin: 0px;
    font-weight: 600;
    font-size: 0.875rem;
    line-height: 1.57143;
    color: rgb(33, 43, 54);
`
const SsubTittle = styled.p`
    margin: 0px;
    font-weight: 400;
    font-size: 0.875rem;
    line-height: 1.57143;
    color: rgb(99, 115, 129);
`
export {
  CardContainer,
  SCard,
  BoxText,
  Stitle,
  SsubTittle
}
