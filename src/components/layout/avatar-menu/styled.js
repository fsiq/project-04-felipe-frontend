import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import styled from 'styled-components'

const Button = styled.button`
  background-color: Transparent;
  background-repeat:no-repeat;
  border: none;
  overflow: hidden;
`

const MenuHeader = styled.div`
    margin-top: 12px;
    margin-bottom: 12px;
    padding-left: 20px;
    padding-right: 20px;
`

const UserNameHeader = styled.h6`
    margin: 0px;
    font-weight: 600;
    font-size: 1rem;
    line-height: 1.5;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`
const UserEmailHeader = styled.p`
    margin: 0px;
    font-family: "Public Sans", sans-serif;
    font-weight: 400;
    font-size: 0.875rem;
    line-height: 1.57143;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: rgb(99, 115, 129);
`
const STypography = styled(Typography)`
  margin-left: 7px;
`
const MenuItemBuntton = styled(MenuItem)`
  padding: 12px 16px;
`
const LogoutButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align:center;
  box-sizing: border-box;
  -webkit-tap-highlight-color: transparent;
  background-color: transparent;
  outline: 0px;
  margin: 0px;
  cursor: pointer;
  user-select: none;
  vertical-align: middle;
  appearance: none;
  text-decoration: none;
  font-weight: 700;
  font-size: 0.875rem;
  text-transform: capitalize;
  min-width: 64px;
  padding: 5px 15px;
  border-radius: 8px;
  color: inherit;
  width: 100%;
  border: 1px solid rgba(145, 158, 171, 0.32);
`
export {
  Button,
  MenuHeader,
  UserNameHeader,
  UserEmailHeader,
  STypography,
  LogoutButton,
  MenuItemBuntton
}
