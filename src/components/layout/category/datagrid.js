// import { useState } from 'react'
import React, { useState } from 'react'

import { DataGrid } from '@material-ui/data-grid'

import Actions from '~/components/layout/category/actions'

const DataList = ({ data, loading }) => {
  const [idRow, setIdRow] = useState('')

  const handleGetIdRow = (id) => {
    setIdRow(id)
  }

  const viewImageColumn = (props) => {
    return <img alt="image category" src={process.env.REACT_APP_STATIC + props.value} />
  }

  const columns = [
    { field: 'image', headerName: 'image', disableColumnMenu: true, renderCell: viewImageColumn },
    { field: 'id', headerName: 'ID', disableColumnMenu: true, flex: 1 },
    { field: 'name', headerName: 'Category Name', disableColumnMenu: true, flex: 1 },
    { field: 'createdAt', headerName: 'Create at', disableColumnMenu: true, flex: 1 },
    { field: 'status', headerName: 'Status', disableColumnMenu: true, flex: 1 },
    {
      field: 'sss',
      headerName: ' ',
      disableColumnMenu: true,
      width: 80,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <Actions data={params} idRemove={idRow} />
      )
    }
  ]
  if (loading) {
    return <p>Carregando</p>
  }
  return (
    <div style={{ height: 600, width: '100%' }}>
      <DataGrid
        rows={data}
        columns={columns}
        pageSize={9}
        checkboxSelection
        disableSelectionOnClick
        onRowClick={(item) => handleGetIdRow(item.id)}
      />
    </div>
  )
}

export default DataList
