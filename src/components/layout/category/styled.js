import styled from 'styled-components'
import { Link } from '@reach/router'

const MenuLink = styled(Link)`
  text-decoration: none;
  color: #000;
  margin-left: 10px;
`
const DivActions = styled.div`
  display: flex;
`
const P = styled.p`
  margin-left: 10px;
`

export {
  MenuLink,
  DivActions,
  P
}
