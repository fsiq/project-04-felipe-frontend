import { NavBar, SMenuIcon, NavIcon, NavRight, Bell } from '~/components/layout/dashboard/navbar/styled'
import AvatarMenu from '~/components/layout/admin/avatar-menu'

import SearchIcon from '@material-ui/icons/Search'
import NotificationsIcon from '@material-ui/icons/Notifications'

const Navbar = ({ sideBar, toggle }) => {
  return (

    <NavBar>
      <NavIcon>
        {!sideBar ? <SMenuIcon onClick={() => toggle()} /> : ''}
        {!sideBar ? <SearchIcon /> : ''}
      </NavIcon>

      <NavRight>
        {!sideBar ? <Bell><NotificationsIcon /></Bell> : ''}
        <AvatarMenu sideBar={sideBar} />
      </NavRight>
    </NavBar>
  )
}

export default Navbar
