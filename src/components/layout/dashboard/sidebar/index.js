import {
  SideBarTitle, SideDivBarImg, SideBarImg, SHighlightOffIcon,
  SideBarMenu, SideBarMenuLink, SideBarLink, SideBarTitleDrop, SideArrow, DropdownUl,
  DropdownItem, DropdownSubItem, DropdownLink
} from './styled'

import LogoHathor from '~/assets/images/logo-sem-nome-preview.png'

// components
import AdminPerfilCard from '~/components/layout/admin/cards/AdminPerfilCard'
// icons
import SpeedIcon from '@material-ui/icons/Speed'
import PersonIcon from '@material-ui/icons/Person'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import CategoryIcon from '@material-ui/icons/Category'
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation'
import { useState } from 'react'
import { useSelector } from 'react-redux'

const SideBar = ({ sideBar, toggle, location }) => {
  const [perfilDrop, setPerfilDrop] = useState(false)
  const [categoryDrop, setCategoryDrop] = useState(false)
  const [paternsDrop, setPaternsDrop] = useState(false)

  const togglePerfilDrop = () => setPerfilDrop(!perfilDrop)
  const toggleCategoryDrop = () => setCategoryDrop(!categoryDrop)
  const togglePaternsDrop = () => setPaternsDrop(!paternsDrop)

  const user = useSelector((state) => state.auth.usuario)

  return (
    <div className={sideBar ? 'sidebar-responsive' : ''} id="sidebar">
      <SideBarTitle>
        <SideDivBarImg>
          <SideBarImg src={LogoHathor} />
          {sideBar ? <SHighlightOffIcon onClick={() => toggle()} /> : ''}
        </SideDivBarImg>
      </SideBarTitle>
      <AdminPerfilCard />
      <SideBarMenu>
        <h2>Geral</h2>
        <SideBarMenuLink isActive={location.pathname === '/admin/'}>
          <SpeedIcon />
          <SideBarLink to="/admin/">App</SideBarLink>
        </SideBarMenuLink>
        {/* <SideBarMenuLink className="">
          <ShowChartIcon />
          <SideBarLink to="#">Analttics</SideBarLink>
        </SideBarMenuLink> */}

        <h2>Gerenciar</h2>
        {/* SETTINGS USUARIO */}
        <DropdownUl onClick={togglePerfilDrop}>
          <DropdownItem>
            <PersonIcon />
            <SideBarTitleDrop>User</SideBarTitleDrop>
            <SideArrow />
          </DropdownItem>
          <DropdownSubItem className={perfilDrop ? '' : 'dropdown-sub--item'}>
            <li><DropdownLink to="/admin/users/list">List</DropdownLink></li>
            {/* <li><DropdownLink to="/admin/users">Account</DropdownLink></li> */}
          </DropdownSubItem>
        </DropdownUl>

        {/* PARCEIRO */}

        <DropdownUl onClick={togglePaternsDrop}>
          <DropdownItem>
            <AssignmentIndIcon />
            <SideBarTitleDrop>Paterns</SideBarTitleDrop>
            <SideArrow />
          </DropdownItem>
          <DropdownSubItem className={paternsDrop ? '' : 'dropdown-sub--item'}>
            <li><DropdownLink to="/admin/paterns">List</DropdownLink></li>
            {/* FUNCAO DO ASSOCIADO */}
            {/* <li><DropdownLink to="/admin/paterns/list-employee">List employee</DropdownLink></li>
            <li><DropdownLink to="##">Create employee</DropdownLink></li> */}

          </DropdownSubItem>
        </DropdownUl>

        {/* CATEGORIA */}

        <DropdownUl onClick={toggleCategoryDrop}>
          <DropdownItem isActive>
            <CategoryIcon />
            <SideBarTitleDrop>Category</SideBarTitleDrop>
            <SideArrow />
          </DropdownItem>
          <DropdownSubItem className={!categoryDrop ? 'dropdown-sub--item' : ''}>
            <li><DropdownLink to="/admin/category">List</DropdownLink></li>
            <li><DropdownLink to="/admin/category/create">Create</DropdownLink></li>
          </DropdownSubItem>
        </DropdownUl>

        <h2> App </h2>
        <SideBarMenuLink>
          <InsertInvitationIcon />
          <SideBarLink to="/admin/calendar">Calendar</SideBarLink>
        </SideBarMenuLink>
      </SideBarMenu>
    </div>
  )
}

export default SideBar
