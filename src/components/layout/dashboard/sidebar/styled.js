import styled from 'styled-components'
import { Link } from '@reach/router'
import HighlightOffIcon from '@material-ui/icons/HighlightOff'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'

const SideBarTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 30px;
`
const SideDivBarImg = styled.div`
  display: flex;
  align-items: center;
`
const SideBarImg = styled.img`
  width: 65px;
  object-fit: contain;
  border-radius: 50%;
`
const SHighlightOffIcon = styled(HighlightOffIcon)`
  position: absolute; 
  top: 37px;
  right: 20px;
`
const SideBarMenu = styled.div`
  h2 {
    font-size: 16px;
    margin-top: 15px;
    margin-bottom: 5px;
    padding: 0 10px;
    font-weight: 700;
  }
`
const SideBarMenuLink = styled.ul`
  padding: 10px;
  display: flex;
  align-items: center;
  border-radius: 3px;
  margin-left: 20px;
  margin-bottom: 5px;
  ${(props) => props.isActive && `
    width: 100%;
    transition-delay: 250ms;
    transition-property: background, border-right;
    background: rgba(62, 161, 117, 0.1);
    border-right: 3px solid rgba(62, 161, 117, 1);
    color: #3ea175
  `}
`
const SideBarLink = styled(Link)`
  text-decoration: none;
  font-weight: 700;
  margin-left: 14px;
  color: rgb(99, 115, 129);
`

// DROPDOWN
const DropdownUl = styled.ul`
  align-items: center;
`
const DropdownItem = styled.div`
  display: flex;
  border-radius: 3px;
  padding: 10px;
  margin-left: 20px;
  margin-bottom: 5px;
  align-items: center;
`
const DropdownSubItem = styled.ul`
  margin-left: 60px;
  list-style: initial;
  li{
    padding-bottom: 20px;
    &:last-child {
      padding-bottom: 0;
    }
  }
`
const DropdownLink = styled(Link)`
  font-size: 12px;
  text-decoration: none;
  color: rgba(99, 115, 129, 1);
  &:hover{
    color: rgb(0, 0, 0)
  }
`

const SideBarTitleDrop = styled.p`
  text-decoration: none;
  font-size: 14px;
  margin-left: 13px;
  font-weight: 700;
  color: rgb(99, 115, 129);
`
const SideArrow = styled(ArrowForwardIosIcon)`
  width: 12px;
  height: 12px;
  position: absolute;
  left: 245px;
  @media only screen and (max-width: 978px) {
    left: 235px
  }
`

export {
  SideBarTitle,
  SideDivBarImg,
  SideBarImg,
  SHighlightOffIcon,
  SideBarMenu,
  SideBarMenuLink,
  SideBarLink,
  SideBarTitleDrop,
  SideArrow,
  DropdownUl,
  DropdownItem,
  DropdownSubItem,
  DropdownLink
}
