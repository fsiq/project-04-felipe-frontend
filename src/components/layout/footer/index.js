import { RiFacebookFill, RiLinkedinFill, RiGithubFill, RiGitlabFill, RiPhoneFill, RiMapPin2Fill } from 'react-icons/ri'
import { MdEmail } from 'react-icons/md'
import {
  SFooter,
  ListContact,
  Stitle,
  Hr,
  SLink,
  SQLink,
  Copyright,
  Logo
} from '~/components/layout/footer/styled'
import Hathor from '../../../assets/images/hathor_Logotipo-removebg-preview.png'

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css" integrity="sha384-wESLQ85D6gbsF459vf1CiZ2+rr+CsxRY0RpiF1tLlQpDnAgg6rwdsUF1+Ics2bni" crossOrigin="anonymous" />

const Footer = () => {
  return (
    <>
      <SFooter>
        <Hr />
        <div className="container-footer">
          <div className="sec aboutus">
            <Stitle>Sobre</Stitle>
            <p>Exercitation sunt ullamco irure elit. Dolore exercitation mollit cillum aliqua do pariatur qui Lorem est duis dolore mollit ut commodo. Elit mollit esse quis do adipisicing consequat dolor ex. Culpa sit ad id ut eiusmod velit aliquip exercitation culpa labore sunt proident. Laboris ea fugiat nulla occaecat sunt irure aliquip eu nostrud pariatur duis ad labore magna. Culpa sint excepteur nostrud eu fugiat. Amet qui nisi mollit anim nisi voluptate aute sit laboris duis tempor ea.</p>
            <ul className="sci">
              <li><SLink to="#"><RiFacebookFill /></SLink></li>
              <li><SLink to="#"><RiLinkedinFill /></SLink></li>
              <li><SLink to="#"><RiGithubFill /></SLink></li>
              <li><SLink to="#"><RiGitlabFill /></SLink></li>
            </ul>
          </div>
          <div className="sec quick-links">
            <Stitle>Hathor</Stitle>
            <ul>
              <li><SQLink to="#">Home</SQLink></li>
              <li><SQLink to="#">Barbearia e Salão</SQLink></li>
              <li><SQLink to="#">Carreiras</SQLink></li>
              <li><SQLink to="#">Hathor Empresas</SQLink></li>
              <li><SQLink to="#">Termos e Condições de uso</SQLink></li>
              <li><SQLink to="#">Contato</SQLink></li>
            </ul>
          </div>
          <div className="sec contact">
            <Stitle>Contato</Stitle>
            <ul className="info">
              <ListContact><span><RiMapPin2Fill /></span>Rj - Rio de Janeiro</ListContact>
              <ListContact><span><RiPhoneFill /></span><SQLink to="tel: 0000000000000">(00)00000-0000</SQLink></ListContact>
              <ListContact><span><MdEmail /></span><SQLink to="mailto:felipe.msiqueira@al.infnet.edu.br">felipe.msiqueira@al.infnet.edu.br</SQLink></ListContact>
            </ul>
          </div>
        </div>
      </SFooter>
      <Copyright>
        <Hr />
        <Logo src={Hathor} />
        <p>Copyright © 2021 Felipe Siqueira. All Righs Reserverd.</p>
      </Copyright>
    </>
  )
}

export default Footer
