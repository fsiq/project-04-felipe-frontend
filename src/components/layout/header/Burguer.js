import React, { useState } from 'react'
import styled from 'styled-components'
import MenuToggle from './menuToggle'
import { motion } from 'framer-motion'
import MenuLinks from './menuLinks'

const BurgerContainer = styled.div`
  display: flex;
`
const MenuContainer = styled(motion.div)`
  width: 100%;
  height: 100%;
  background-color: #fff;
  z-index: 90;
  position: fixed;
  top: 0;
  right: 0;
  user-select: none;
  padding: 1em 2.5em;
`
const BurguerIcon = styled.div`
  color: black;
  position: fixed;
  cursor: pointer;
  z-index: 99;
`
const menuVariants = {
  open: {
    transform: 'translateX(0%)',
  },
  closed: {
    transform: 'translateX(110%)',
  }
}

// const menuTransition = { type: 'spring', duration: 1, stiffness: 33, delay: 0.1 }

const Burguer = (props) => {
  const [isOpen, setOpen] = useState(false)
  const toggleMenu = () => {
    setOpen(!isOpen)
  }

  return (
    <BurgerContainer>
      <BurguerIcon>
        <MenuToggle toggle={toggleMenu} isOpen={isOpen} />
        <MenuContainer initial={false} animate={isOpen ? 'open' : 'closed'} variants={menuVariants} transition={{ type: 'spring' }}>
          <MenuLinks toggle={toggleMenu} isOpen={isOpen} />
        </MenuContainer>
      </BurguerIcon>
    </BurgerContainer>
  )
}

export default Burguer
