import Navbar from '~/components/layout/header/navbar/index'
import Burguer from './Burguer'

const Header = () => {
  return (
    <div className="nav-container">
      <Burguer />
      <Navbar />
    </div>
  )
}

export default Header
