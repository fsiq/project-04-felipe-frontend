import styled from 'styled-components'
import { useSelector } from 'react-redux'
import { Link } from '@reach/router'
import { MdClose } from 'react-icons/md'

import Hathor from '../../../assets/images/hathor_Logotipo-removebg-preview.png'

const MenuContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
    img {
      width: 163px;
      height: 43;
      margin: 0;
      display: block;
      position: absolute;
      top: -100px;
      right: 80px;
      transform: translate(-100%, 75px);

    }
    ul {
      display: flex;
      width: 100%;
      flex-flow: column nowrap;
      list-style: none;
      margin-top: 100px;
      li {
        display: flex;
        justify-content: space-between;
        padding: 10px;
        justify-content: center;
        :hover {
          text-decoration: underline;
        }
        
      }
      .sign-up {
        color: darkblue;
      }
    }
`
const Button = styled.div`
  z-index: 99;
  cursor: pointer;
`

const Close = styled(MdClose)`
display: none;

@media screen and (max-width:768px) {
  display: block;
  position: absolute;
  top: -50px;
  right: 0px;
  transform: translate(-100%, 75px);
  font-size: 1.8rem;
  cursor: pointer;
}
`
const SLink = styled(Link)`
    color: black;
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 18px;
    padding: 0 1rem;
    height: 100%;
    cursor: pointer;
`
const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-top: 40px;
  margin-right: 24px;
  width: 100%;
`
const NavBtnLink = styled(Link)`
  border-radius: 4px;
  text-align: center;
  background-color: #171926;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  width: 100%;
  text-decoration: none;
`

const MenuLinks = ({ toggle, isOpen }) => {
  const user = useSelector((state) => state.auth.usuario)
  return (
    <MenuContainer>
      <div className="div-logo">
        <img src={Hathor} />
      </div>
      <Button onClick={toggle}>
        {isOpen ? <Close /> : ''}
      </Button>
      <ul>
        <li><SLink to="/">Home</SLink></li>
        <li><SLink to="#"> Barbearia e Salão </SLink></li>
        <li><SLink to="#">Carreiras</SLink></li>
        <li><SLink to="#">Hathor Empresas</SLink></li>
        <NavBtn>
          <NavBtnLink to="/signin">Entrar</NavBtnLink>
        </NavBtn>
        <li><SLink to="#" className="sign-up">criar conta</SLink></li>
      </ul>
    </MenuContainer>
  )
}

export default MenuLinks
