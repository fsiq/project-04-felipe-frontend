import styled from 'styled-components'
import { FaBars } from 'react-icons/fa'
import { MdClose } from 'react-icons/md'

const Button = styled.div`
  z-index: 99;
  cursor: pointer;
`
const Bars = styled(FaBars)`
  display: none;

  @media screen and (max-width:768px) {
    display: block;
    position: absolute;
    top: -50px;
    left: 50px;
    transform: translate(-100%, 75px);
    font-size: 1.8rem;
    cursor: pointer;
  }
`
const Close = styled(MdClose)`
  display: none;

  @media screen and (max-width:768px) {
    display: block;
    position: absolute;
    top: -50px;
    right: 0px;
    transform: translate(-100%, 75px);
    font-size: 1.8rem;
    cursor: pointer;
  }
`

const MenuToggle = ({ toggle, isOpen }) => {
  return (
    <Button onClick={toggle}>
      {isOpen ? <Close /> : <Bars />}
    </Button>

  )
}

export default MenuToggle
