/* eslint-disable react/jsx-closing-tag-location */
import { useState } from 'react'
import { useSelector } from 'react-redux'
import Hathor from '~/assets/images/hathor_Logotipo-removebg-preview.png'
import { SLink, NavBtn, NavBtnLink } from '~/components/layout/header/navbar/styled'

import { isAuthenticated } from '~/config/storage'

import AvatarMenu from '~/components/layout/avatar-menu'
const Navbar = () => {
  const user = useSelector((state) => state.auth.usuario)

  const [navBar, setNavBar] = useState(false)

  const changeBackGround = () => {
    if (window.scrollY >= 80) {
      setNavBar(true)
    } else {
      setNavBar(false)
    }
  }

  window.addEventListener('scroll', changeBackGround)

  return (
    <div className="navbar">
      <div className={navBar ? 'navbar-container active' : 'navbar-container'}>
        <div className="logo-nav">
          <img src={Hathor} alt="Hathor Logo" />
        </div>
        <ul className="nav-ul">
          <li><SLink to="/">Home</SLink></li>
          <li><SLink to="#">Barbearia e Salão</SLink></li>
          <li><SLink to="#">Carreiras</SLink></li>
          <li><SLink to="/signup-partner">Seja um Parceiro</SLink></li>
          {isAuthenticated() ? '' : <li><SLink to="/signup" className="sign-up">criar conta</SLink></li>}
        </ul>
        {isAuthenticated()
          ? <AvatarMenu isAuthenticate={user} />
          : <NavBtn>
            <NavBtnLink to="/signin">Entrar</NavBtnLink>
          </NavBtn>}
      </div>
    </div>
  )
}
export default Navbar
