import { Link } from '@reach/router'
import styled from 'styled-components'

const SLink = styled(Link)`
    color: #000;
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 16px;
    font-weight: 700;
    padding: 0 1.1rem;
    height: 100%;
    cursor: pointer;
    &:hover {
      color: black;
    }
    &.sign-up {
      font-style: italic;
      position: relative;
      right:-20px;
    }
    &.sign-up:hover {
      color: darkslateblue;
    }
`
const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 10px;
`
const NavBtnLink = styled(Link)`
  border-radius: 4px;
  position: relative;
  right: -10px;
  background-color: #171926;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  @media only screen and (max-width: 978px) {
    right: 20px;
  }
`

export {
  SLink,
  NavBtn,
  NavBtnLink
}
