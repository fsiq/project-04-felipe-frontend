import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

const DeleteModal = ({ open, close, remove }) => {
  return (
    <div>
      <Dialog
        open={open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Remover</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            deseja remover o item selecionado ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <button onClick={close} color="primary">
            Não
          </button>
          <button onClick={remove} color="primary" autoFocus>
            Sim
          </button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default DeleteModal
