import styled from 'styled-components'
import { Link } from '@reach/router'

const SFooter = styled.footer`
  position: relative;
  width: 100%;
  height: auto;
  padding: 50px 20px;
  background-color: #fff;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  @media screen and (min-width:1080px) {
    padding: 50px 100px;
  }
  .container-footer {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    flex-direction: row;
    @media screen and (max-width:768px) {
      flex-direction: column;
    }
    .sec {
      margin-right: 30px;
      @media screen and (max-width:768px) {
      margin-right: 0;
      margin-bottom: 40px;
    }
      &.aboutus {
        width: 40%;
        @media screen and (max-width:768px) {
          width: 100%;
        }
      }
      .sci {
        margin-top: 20px;
        display: flex;
        & li {
          list-style: none;
        }
      }
      &.quick-links {
        position: relative;
        width: 20%;
        @media screen and (max-width:768px) {
          width: 100%;
        }
        &.quick-links ul li {
          list-style: none;
        }
      }
      &.contact {
        width: calc(40% -30px);
        margin-right: 0 !important;
        @media screen and (max-width:768px) {
          width: 100%;
          margin-bottom: 0;
        }
        &.info {
          position: relative;
        }
      }
    }
  }
`
const ListContact = styled.li`
  display: flex;
  margin-bottom: 16px;
  & span a{
    text-decoration: none;
    color: black
  }
`
const Stitle = styled.h2`
  position: relative;
  color: #3E3E3E;
  font-weight: 500 bold;
  margin-bottom: 15px;
  &:before {
    content: '';
    position: absolute;
    bottom: -5px;
    left: 0;
    width: 50px;
    height: 2px;
    background-color: #171926;
  }
  &p {
    color: 717171
  }
`
const Hr = styled.hr`
  width: 100%;
  margin-bottom: 15px;
  border-color: rgba(71, 71, 71, 0.2);
`
const SLink = styled(Link)`
  display: inline-block;
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #717171;
  margin-right: 10px;
  text-decoration: none;
  &:hover {
    color: #000;
  }
`
const SQLink = styled(Link)`
  display: inline-block;
  margin-bottom: 10px;
  display: flex;
  color: #717171;
  text-decoration: none;
  &:hover {
    color: #000;
  }
`
const Copyright = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  padding: 50px 20px;
  background-color: #fff;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  @media screen and (min-width:1080px) {
    padding: 50px 100px;
  }
  @media screen and (max-width:768px) {
  width: 100%;
  justify-content: center;
  margin-bottom: 0;
  }
`
const Logo = styled.img`
  width: 100px;
  height: 100px;
`

export {
  SFooter,
  ListContact,
  Stitle,
  Hr,
  SLink,
  SQLink,
  Copyright,
  Logo
}
