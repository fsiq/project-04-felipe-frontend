import styled from 'styled-components'
import Header from './header'
import Footer from './footer'

const Layout = ({ children }) => {
  return (
    <ContainerLayout>
      <Header />
      {children}
      <Footer />
    </ContainerLayout>
  )
}

export default Layout

const ContainerLayout = styled.div``
