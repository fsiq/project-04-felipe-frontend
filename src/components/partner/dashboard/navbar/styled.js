import styled from 'styled-components'
import MenuIcon from '@material-ui/icons/Menu'

const NavBar = styled.div`
  background: #fff;
  grid-area: nav;
  height: 60px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  padding: 0 30px 0 30px;
  @media only screen and (max-width: 978px) {
    position: sticky;
    top: 0;
  }
`
const NavIcon = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
const NavRight = styled.div`
  display: flex;
  align-items: center;
  padding: 1rem;
`
const Bell = styled.button`
  display: flex;
  align-items: center;
  margin-right: 20px;
  color: #000;
  background-color: Transparent;
  background-repeat:no-repeat;
  border: none;
  cursor: pointer;
`

const SMenuIcon = styled(MenuIcon)`
  display: none;

  @media only screen and (max-width: 978px) {
      display: block;
      margin-right: 24px;
  }
`
const HideButton = styled.button`
  background-color: Transparent;
  background-repeat:no-repeat;
  border: none;
  cursor: pointer;
  overflow: hidden;
`

export {
  NavBar,
  NavIcon,
  NavRight,
  SMenuIcon,
  HideButton,
  Bell
}
