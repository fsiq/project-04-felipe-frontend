import {
  SideBarTitle, SideDivBarImg, SideBarImg, SHighlightOffIcon,
  SideBarMenu, SideBarMenuLink, SideBarLink
} from './styled'

import LogoHathor from '~/assets/images/logo-sem-nome-preview.png'

// components
import AdminPerfilCard from '~/components/layout/admin/cards/AdminPerfilCard'
// icons
import SpeedIcon from '@material-ui/icons/Speed'
import GroupIcon from '@material-ui/icons/Group'
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation'
import { useState } from 'react'
import { useSelector } from 'react-redux'

const SideBar = ({ sideBar, toggle, location }) => {
  const [perfilDrop, setPerfilDrop] = useState(false)
  const [categoryDrop, setCategoryDrop] = useState(false)
  const [paternsDrop, setPaternsDrop] = useState(false)

  const togglePerfilDrop = () => setPerfilDrop(!perfilDrop)
  const toggleCategoryDrop = () => setCategoryDrop(!categoryDrop)
  const togglePaternsDrop = () => setPaternsDrop(!paternsDrop)

  const user = useSelector((state) => state.auth.usuario)

  return (
    <div className={sideBar ? 'sidebar-responsive' : ''} id="sidebar">
      <SideBarTitle>
        <SideDivBarImg>
          <SideBarImg src={LogoHathor} />
          {sideBar ? <SHighlightOffIcon onClick={() => toggle()} /> : ''}
        </SideDivBarImg>
      </SideBarTitle>
      <AdminPerfilCard />
      <SideBarMenu>
        <h2>Geral</h2>
        <SideBarMenuLink isActive={location.pathname === '/partner'}>
          <SpeedIcon />
          <SideBarLink to="/partner">App</SideBarLink>
        </SideBarMenuLink>

        <SideBarMenuLink isActive={location.pathname === '/partner/employee' || location.pathname === '/partner/employee/create'}>
          <GroupIcon />
          <SideBarLink to="/partner/employee">Employee</SideBarLink>
        </SideBarMenuLink>

        <SideBarMenuLink>
          <InsertInvitationIcon />
          <SideBarLink to="#">Calendar</SideBarLink>
        </SideBarMenuLink>

      </SideBarMenu>
    </div>
  )
}

export default SideBar
