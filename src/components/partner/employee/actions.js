/* eslint-disable react/jsx-indent */
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { removeEmployee } from '~/store/partner/partner.action'
import {
  Button
} from '@material-ui/core'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import DeleteIcon from '@material-ui/icons/Delete'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import EditIcon from '@material-ui/icons/Edit'

import DeleteModal from '~/components/layout/modal/delete'
import { MenuLink, DivActions, P } from '~/components/layout/category/styled'

const Actions = (props) => {
  const dispatch = useDispatch()

  const { partnersID } = props.data?.row

  const [anchorEl, setAnchorEl] = useState(null)

  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(!open)
  }

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleRemove = () => {
    dispatch(removeEmployee(props.idRemove, partnersID))
    setOpen(false)
  }
  return (
    <>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreVertIcon />
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem>
          <MenuLink
            to={`/admin/category/${props.data.row.name}/edit`}
            state={{ category: props.data.row }}
          >
            <DivActions>
              <EditIcon />
              <P>Edit</P>
            </DivActions>
          </MenuLink>
        </MenuItem>

        <MenuItem>
          <DivActions onClick={handleOpen}>
            <DeleteIcon />
            <P>Delete</P>
          </DivActions>
          {open ? <DeleteModal open close={handleOpen} remove={handleRemove} /> : ''}
        </MenuItem>
      </Menu>
    </>
  )
}

export default Actions
