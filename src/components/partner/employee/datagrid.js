import React, { useState } from 'react'

import { DataGrid } from '@material-ui/data-grid'

import Actions from '~/components/partner/employee/actions'
import Avatar from '@material-ui/core/Avatar'

const DataList = ({ data, loading }) => {
  const [idRow, setIdRow] = useState('')

  const handleGetIdRow = (id) => {
    setIdRow(id)
  }

  const viewImageColumn = (props) => {
    return <Avatar alt="image category" src={process.env.REACT_APP_STATIC + props.value} />
  }

  const columns = [
    { field: 'id', headerName: 'ID', disableColumnMenu: true, width: 300 },
    { field: 'image', headerName: 'image', disableColumnMenu: true, renderCell: viewImageColumn },
    { field: 'name', headerName: 'Nome Completo', disableColumnMenu: true, width: 300 },
    { field: 'categoryName', headerName: 'Categoria', disableColumnMenu: true, width: 200 },
    { field: 'phone', headerName: 'Telefone', disableColumnMenu: true, width: 200 },
    { field: 'createdAt', headerName: 'Create at', disableColumnMenu: true, width: 250 },
    { field: 'status', headerName: 'Status', disableColumnMenu: true, flex: 1 },
    {
      field: 'sss',
      headerName: ' ',
      disableColumnMenu: true,
      width: 80,
      // eslint-disable-next-line react/display-name
      renderCell: (params) => (
        <Actions data={params} idRemove={idRow} />
      )
    }
  ]
  if (loading) {
    return <p>Carregando</p>
  }
  return (
    <div style={{ height: 600, width: '100%' }}>
      <DataGrid
        rows={data}
        columns={columns}
        pageSize={9}
        checkboxSelection
        disableSelectionOnClick
        onRowClick={(item) => handleGetIdRow(item.id)}
      />
    </div>
  )
}

export default DataList
