/* eslint-disable react/jsx-key */
import React, { useEffect, useCallback } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { getAll as getCategories } from '~/store/category/category.action'

import { Card, CardContainer, BigCard, BigCardContent, Title, Img, LinkDiv, NextIcon } from './styled'

export default function CategoryCard() {
  const dispatch = useDispatch()

  const callCategory = useCallback(() => {
    dispatch(getCategories())
  }, [dispatch])

  useEffect(() => {
    callCategory()
  }, [callCategory])

  const categories = useSelector((state) => state.category.all)

  const data = categories.map((item) => {
    return item
  })

  return (
    <>
      <Card>
        <CardContainer>
          <BigCard>
            <BigCardContent>
              <Title>{data[0]?.name}</Title>
              <Img src={process.env.REACT_APP_STATIC + data[0]?.image} />
              <LinkDiv>
                <span>Saiba mais</span>
                <NextIcon />
              </LinkDiv>
            </BigCardContent>
          </BigCard>
          <BigCard issalao="true">
            <BigCardContent>
              <Title issalao="true">{data[1]?.name}</Title>
              <Img src={process.env.REACT_APP_STATIC + data[1]?.image} issalao="true" />
              <LinkDiv>
                <span>Saiba mais</span>
                <NextIcon />
              </LinkDiv>
            </BigCardContent>
          </BigCard>
        </CardContainer>
      </Card>
    </>
  )
}
