import styled from 'styled-components'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

export const NextIcon = styled(NavigateNextIcon)`

`

export const Card = styled.div`
  width: 90%;
  max-width: 842px;
  /* background-color: red; */
`

export const CardContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 200px;
  width: 100%;
`

export const BigCardContent = styled.div`
  display: flex;
  height: 100%;
  width: 90%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const BigCard = styled.div`
  width: 38%;
  transition: opacity 1.5s ease;
  height: 100%;
  cursor: pointer;
  border-radius: 20px;
  background-color: #D9A84E;
  &:first-child {
    margin-right: 25px;
  }
  ${(props) => props.issalao && `
    background-color: #8C60C5;
  `}
  @media only screen and (max-width: 978px) {
    width: 100%;
  }
`

export const Title = styled.p`
  font-size: 2.25rem;
  font-weight: 500;
  /* margin: 0 0 10px 0; */
  color: #fff;
  position: relative;
  top: 25px;
  right: 50px;
  ${(props) => props.issalao && `
    right: 85px;
  `}
  @media only screen and (max-width: 978px) {
    font-size: 1.25rem;
    top: 0px;
    right: 0px;
  }
`

export const Img = styled.img`
height: 160px;
position: relative;
left: 85px;
@media only screen and (max-width: 978px) {
height: 140px;
left: 0px;
${(props) => props.issalao && `
  left: 10px;
`}
}
`

export const LinkDiv = styled.div`
  display: flex;
  font-weight: 500;
  font-size: 1rem;
  position: relative;
  top: -40px;
  right: 70px;
  padding: 5px;
  background-color: rgba(0,0,0, 0.1);
  color: #fff;
  border-radius: 20px;
  & span {
    overflow-x: hidden;
    font-size: 16px;
    font-weight: 700;
  }
  @media only screen and (max-width: 978px) {
    top: 0px;
    right: -8px;
  }
`
