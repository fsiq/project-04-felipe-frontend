import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import ButtonBase from '@material-ui/core/ButtonBase'
import Avatar from '@material-ui/core/Avatar'
import Rating from '@material-ui/lab/Rating'

import { SFavoriteIcon } from './styled'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 500,
  },
  image: {
    width: 128,
    height: 128,
  },
  img: {
    margin: 'auto',
    display: 'block',
    width: '100%',
    height: '100%',
    maxWidth: '100%',
    maxHeight: '100%',
  },
}))

export default function ComplexGrid({ data }) {
  const classes = useStyles()
  console.log(data)

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image}>
              <Avatar className={classes.img} alt="complex" src={process.env.REACT_APP_STATIC + data.image} />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant="h6">
                  {data.companyName}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  {data.address.toLowerCase() + ' / ' + data.uf.toLowerCase()}
                </Typography>
                <Rating name="size-small" defaultValue={data.likes} size="small" readOnly />
              </Grid>
              <Grid item>
                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                  Saiba mais
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <SFavoriteIcon />
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}
