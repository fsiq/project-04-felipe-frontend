import axios from 'axios' // import da dependencia
import { getToken } from './storage'
import { navigate } from '@reach/router'
import store from '../store'
import { logoutAction } from '../store/auth/auth.action'

// definindo a url da api
const urlApi = process.env.REACT_APP_API

// criando um client http através do AXIOS
const http = axios.create({
  baseURL: urlApi
})

// Definindo o header padrão da aplicação
http.defaults.headers['content-type'] = 'application/json'
if (getToken()) {
  http.defaults.headers.token = getToken()
}

http.interceptors.response.use(
  (response) => response,
  (error) => {
    switch (error.response.status) {
      case 401:
        store.dispatch(logoutAction())
        navigate('/signin')
        break
      default:
        break
    }
  }
)

export default http
