import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#171926',
    },
    secondary: {
      main: '#00ab55',
    },
    third: {
      main: '#8C4E03',
    },
    fourth: {
      main: '#F2C094',
    },
    fifth: {
      main: '#A65D56',
    },
    greenSwitch: {
      main: '#00ab55',
    }
  },
})

export default theme
