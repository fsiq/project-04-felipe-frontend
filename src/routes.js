import React from 'react'
import { Router, Redirect } from '@reach/router'
import { useSelector } from 'react-redux'
// config
import Signin from '~/view/auth/signin'
import Signup from '~/view/auth/singup'
import SignupPartner from '~/view/auth/singup-partner'
import PortalView from '~/view/portal/'
import AdminView from '~/view/admin'
import PartnerView from '~/view/partner'
import List from '~/view/list'
import { isAuthenticated } from '~/config/storage'

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/signin" noThrow />
  }
  return <Component {...rest} />
}

const AdminRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/signin" noThrow />
  }
  const hasAdmin = Object.keys(rest).includes('admin') && !rest.admin

  if (hasAdmin) {
    return <Redirect to="/" noThrow />
  }

  return <Component {...rest} />
}

const PartnerRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/signin" noThrow />
  }
  const hasPartner = Object.keys(rest).includes('partner') && !rest.partner

  if (hasPartner) {
    return <Redirect to="/" noThrow />
  }
  return <Component {...rest} />
}

const Routes = () => {
  const isAdmin = useSelector(state => state.auth.admin)
  const isPartner = useSelector(state => state.auth.partner)
  return (
    <>
      <Router>
        <Signin path="signin" />
        <Signup path="signup" />
        <SignupPartner path="signup-partner" />
        {/* Public */}
        <PortalView path="/*" />
        <List path="lista-employe" />
        {/* Partner */}
        <PartnerRoute component={PartnerView} path="/partner/*" partner={isPartner} />
        {/* Admin */}
        <AdminRoute component={AdminView} path="/admin/*" admin={isAdmin} />
      </Router>
    </>
  )
}

export default Routes
