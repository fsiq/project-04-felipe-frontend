import http from '~/config/http'

const authService = (data) => http.post('/auth', data)
const signUpService = (data) => http.post('/users', data)
const SignUpPartnerService = (data) => http.post('/partners', data)

export { authService, signUpService, SignUpPartnerService }
