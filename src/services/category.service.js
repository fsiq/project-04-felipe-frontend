import http from '~/config/http'

const getAllCategories = () => http.get('/category')
const getCategoriesById = (id) => http.get(`/category/${id}`)
const CategoryCreateService = (data, config = {}) => http.post('/category', data, config)
const removeCategory = (id) => http.delete(`/category/${id}`)
const updateCategory = (id, data, config = {}) => http.put(`/category/${id}`, data, config)

export {
  CategoryCreateService,
  getAllCategories,
  getCategoriesById,
  removeCategory,
  updateCategory
}
