import http from '~/config/http'

const listAllPartnerService = () => http.get('/partners')
const listPartnerByIdService = (id) => http.get(`/partners/${id}`)
const createNewPartner = (data) => http.post('/partners', data)
const removePartnerService = (id) => http.delete(`/partner/${id}`)
const updatePartner = (id, data, config = {}) => http.put(`/partner/${id}`, data, config)

/// EMPLOYEE
const listEmployeeByPartner = (id) => http.get(`/employee?partnerId=${id}`)
const createNewEmployee = (id, data, config = {}) => http.post(`/partner/${id}/createEmployee`, data, config)
const removeEmployeeService = (id, partnersID) => http.delete(`/employee/${id}/delete/${partnersID}`)

export {
  listAllPartnerService,
  listPartnerByIdService,
  createNewPartner,
  updatePartner,
  createNewEmployee,
  listEmployeeByPartner,
  removeEmployeeService,
  removePartnerService
}
