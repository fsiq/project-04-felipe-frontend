import http from '~/config/http'

const listAllUsers = () => http.get('/users')
const listUserByIdService = (id) => http.get(`/users/${id}`)
const updateUserService = (id, data, config = {}) => http.put(`/users/${id}`, data, config)
const deleteUserService = (id) => http.delete(`/users/${id}`)

export { listAllUsers, listUserByIdService, updateUserService, deleteUserService }
