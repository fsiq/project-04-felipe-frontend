import { removeToken, saveAuth } from '~/config/storage'
import { authService, signUpService, SignUpPartnerService } from '~/services/auth.service'
import { navigate } from '@reach/router'
import http from '~/config/http'

import TYPES from '~/store/types'
import { toastr } from 'react-redux-toastr'

export const signInAction = (data) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.SIGN_LOADING, status: true })

    try {
      const result = await authService(data)
      if (result.data) {
        saveAuth(result.data?.data)
        http.defaults.headers.token = result.data?.data.token
      }
      dispatch({
        type: TYPES.SIGN_IN, data: result.data?.data
      })
      navigate('/')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Auth', 'Algo está errado')
    }
  }
}

export const signUpAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await signUpService(data)
      dispatch({ type: TYPES.SIGN_UP, data: result.data?.data })
      toastr.success('Sing Up', 'Registrado com Sucesso!')
      navigate('/signin')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Houve um erro ao realizar o cadastro', error)
    }
  }
}

export const signUpPartnerAction = (data) => {
  return async (dispatch) => {
    try {
      const result = await SignUpPartnerService(data)
      dispatch({ type: TYPES.SIGN_UP, data: result.data?.data })
      toastr.success('Sing Up', 'Registrado com Sucesso!, aguarde a avalição do seu registro.')
      setTimeout(() => {
        navigate('/')
      }, 3000)
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Houve um erro ao realizar o cadastro', error)
    }
  }
}

export const logoutAction = (data) => {
  return async (dispatch) => {
    removeToken()
    dispatch({ type: TYPES.SIGN_OUT })
    navigate('/signin')
  }
}
