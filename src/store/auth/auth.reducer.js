import TYPES from '../types'
import { getToken, getUser } from '../../config/storage'

const INITIAL_STATE = {
  admin: getUser().kindUser === 1 || false,
  partner: getUser().kindUser === 2 || false,
  loading: false,
  token: getToken() || '',
  usuario: getUser() || {},
  error: [],
  registered: false
}

const reducer = (state = INITIAL_STATE, action) => { // tamara recebe
  switch (action.type) {
    case TYPES.SIGN_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.SIGN_IN: // disponibiliza na mesa
      state.token = action.data.token
      state.usuario = action.data.userDTO
      state.admin = action.data.userDTO.kindUser === 1
      state.partner = action.data.userDTO.kindUser === 2
      state.loading = false
      return state
    case TYPES.SIGN_UP: // disponibiliza na mesa
      state.registered = true
      state.loading = false
      return state
    case TYPES.SIGN_ERROR: // disponibiliza na mesa
      // const err = [...state.error, action.data]
      state.loading = false
      // state.error = err
      return state
    case TYPES.SIGN_OUT: // disponibiliza na mesa
      state.token = ''
      state.usuario = {}
      state.admin = false
      state.error = []
      return state
    default:
      return state
  }
}

export default reducer
