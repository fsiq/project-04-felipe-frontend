import { toastr } from 'react-redux-toastr'
import { CategoryCreateService, getAllCategories, getCategoriesById, updateCategory, removeCategory } from '~/services/category.service'
import TYPES from '~/store/types'

export const create = (data) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    try {
      const formData = new FormData()
      Object.keys(data).map((k) => formData.append(k, data[k]))
      const result = await CategoryCreateService(formData, config)
      dispatch({ type: TYPES.CATEGORY_CREATE, data: result.data })
      toastr.success('Categoria', 'Categoria cadastrada com sucesso!')
    } catch (error) {
      toastr.error('Categoria', 'Falha ao criar uma nova categoria D:')
    }
  }
}

export const getAll = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.CATEGORY_LOADING, status: true })
      const result = await getAllCategories()
      dispatch({ type: TYPES.CATEGORY_ALL, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const getById = (id) => {
  return async (dispatch) => {
    try {
      const result = await getCategoriesById(id)
      dispatch({ type: TYPES.CATEGORY_EDIT, data: result.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const update = (id, { ...data }) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.CATEGORY_LOADING, status: true })
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    }

    const formData = new FormData()
    Object.keys(data).map((k) => formData.append(k, data[k]))
    try {
      const result = await updateCategory(id, formData, config)
      dispatch({ type: TYPES.CATEGORY_UPDATE, data: result.data })
      dispatch(getById(id))
      toastr.success('Categoria', 'Categoria atualizada com sucesso')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Categoria', error.toString())
    }
  }
}

export const remove = (id) => {
  return async (dispatch) => {
    try {
      const result = await removeCategory(id)
      dispatch({ type: TYPES.CATEGORY_EDIT, data: result.data.data })
      toastr.success('Categoria', 'Removido com sucesso')
      dispatch(getAll())
    } catch (error) {
      toastr.error('aconteceu um erro', error)
      toastr.error('Categoria', error.toString())
    }
  }
}
