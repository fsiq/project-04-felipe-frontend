import TYPES from '~/store/types'
import { toastr } from 'react-redux-toastr'
// import { navigate } from '@reach/router'

import {
  listAllPartnerService, listPartnerByIdService,
  updatePartner, createNewEmployee,
  listEmployeeByPartner, removeEmployeeService, removePartnerService
} from '~/services/partner.service'

export const listAllPartner = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.PARTNER_LOADING, status: true })
      const result = await listAllPartnerService()
      dispatch({ type: TYPES.PARTNER_ALL, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const getById = (id) => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.PARTNER_LOADING, status: true })
      const result = await listPartnerByIdService(id)
      dispatch({ type: TYPES.PARTNER_EDIT, data: result.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const updatePartnerAction = (id, { ...data }) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.PARTNER_LOADING, status: true })
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    }

    const formData = new FormData()
    Object.keys(data).map((k) => formData.append(k, data[k]))
    try {
      const result = await updatePartner(id, formData, config)
      dispatch({ type: TYPES.PARTNER_UPDATE, data: result.data })
      toastr.success('Partner', 'Parceiro atualizado com sucesso')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Partner', error.toString())
    }
  }
}

export const remove = (id) => {
  return async (dispatch) => {
    try {
      const result = await removePartnerService(id)
      dispatch({ type: TYPES.PARTNER_EDIT, data: result.data })
      toastr.success('Categoria', 'Removido com sucesso')
      dispatch(listAllPartner())
    } catch (error) {
      toastr.error('aconteceu um erro', error)
      toastr.error('partner', error.toString())
    }
  }
}

export const getEmployeeById = (id) => {
  return async(dispatch) => {
    dispatch({ type: TYPES.PARTNER_LOADING, status: true })
    try {
      const result = await listEmployeeByPartner(id)
      dispatch({ type: TYPES.PARTNER_EMPLOYIES, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const createNewEmployeeAction = (id, { ...data }) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.PARTNER_LOADING, status: true })
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    }
    const formData = new FormData()
    Object.keys(data).map((k) => formData.append(k, data[k]))
    try {
      const result = await createNewEmployee(id, formData, config)
      dispatch({ type: TYPES.PARTNER_CREATE, data: result.data.data })
      toastr.success('Partner', 'Categoria cadastrada com sucesso!')
    } catch (error) {
      toastr.error('Partner', 'Falha ao criar uma nova categoria D:')
    }
  }
}

export const removeEmployee = (id, partnersID) => {
  return async (dispatch) => {
    try {
      const result = await removeEmployeeService(id, partnersID)
      dispatch({ type: TYPES.PARTNER_EDIT, data: result.data.data })
      toastr.success('Employee', 'Removido com sucesso')
      dispatch(getEmployeeById(partnersID))
    } catch (error) {
      toastr.error('aconteceu um erro', error)
      toastr.error('partner', error.toString())
    }
  }
}
