import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  all: [],
  selected: {},
  employies: []
}
const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.PARTNER_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.PARTNER_ALL:
      state.all = action.data
      state.loading = false
      return state
    case TYPES.PARTNER_EDIT:
      state.selected = action.data
      state.loading = false
      return state
    case TYPES.PARTNER_CREATE:
      state.loading = false
      return state
    case TYPES.PARTNER_EMPLOYIES:
      state.employies = action.data
      state.loading = false
      return state
    default:
      return state
  }
}

export default reducer
