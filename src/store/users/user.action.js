import { toastr } from 'react-redux-toastr'
import TYPES from '~/store/types'

import { listAllUsers as listAllUsersService, listUserByIdService, updateUserService, deleteUserService } from '~/services/user.service'

export const listAllUsers = () => {
  return async (dispatch) => {
    dispatch({ type: TYPES.USER_LOADING, status: true })
    try {
      const result = await listAllUsersService()
      dispatch({ type: TYPES.USER_ALL, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const getById = (id) => {
  return async (dispatch) => {
    dispatch({ type: TYPES.USER_LOADING, status: true })
    try {
      const result = await listUserByIdService(id)
      dispatch({ type: TYPES.USER_EDIT, data: result.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const updateUserAction = (id, { ...data }) => {
  return async(dispatch) => {
    dispatch({ type: TYPES.CATEGORY_LOADING, status: true })
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    }

    const formData = new FormData()
    Object.keys(data).map((k) => formData.append(k, data[k]))
    try {
      const result = await updateUserService(id, formData, config)
      dispatch({ type: TYPES.USER_UPDATE, data: result.data })
      toastr.success('Account', 'Os dados foram atualizados com sucesso')
    } catch (error) {
      dispatch({ type: TYPES.SIGN_ERROR, data: error })
      toastr.error('Account', error.toString())
    }
  }
}

export const deleteUserAction = (id) => {
  return async(dispatch) => {
    try {
      const result = await deleteUserService(id)
      dispatch({ type: TYPES.USER_EDIT, data: result.data.data })
      toastr.success('User', 'Removido com sucesso')
      dispatch(listAllUsers())
    } catch (error) {
      toastr.error('aconteceu um erro', error)
      toastr.error('User', error.toString())
    }
  }
}
