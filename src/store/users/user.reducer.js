import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  all: [],
  selected: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.USER_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.USER_ALL:
      state.all = action.data
      state.loading = false
      return state
    case TYPES.USER_EDIT:
      state.selected = action.data
      state.loading = false
      return state
    default:
      return state
  }
}

export default reducer
