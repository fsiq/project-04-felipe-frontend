import React from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import timeGridPlugin from '@fullcalendar/timegrid'
import listPlugin from '@fullcalendar/list'

import { Container, SubContainer, CreateHeader, CreateHeaderTitle, CreateHeaderUL, NavLink, OtherStyledLink, CustomButtonHeader, Add, Card } from '~/view/admin/category/list/styled'

const Calendar = () => {
  const handleDateClick = (arg) => { // bind with an arrow function
    alert(arg.dateStr)
  }
  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Calendar</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li aria-disabled>Calendar</li>
          </CreateHeaderUL>
          <CustomButtonHeader className="calendar-button--create" calendar>
            <Add />
            <OtherStyledLink to="#">Nova Evento</OtherStyledLink>
          </CustomButtonHeader>
        </CreateHeader>
        <Card>
          <FullCalendar
            plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin, listPlugin]}
            headerToolbar={{
              left: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek',
              center: 'title',
              right: 'prev,today,next'

            }}
            initialView="dayGridMonth"
            editable
            selectable
            selectMirror
            dayMaxEvents
          />
        </Card>
      </SubContainer>
    </Container>

  )
}

export default Calendar
