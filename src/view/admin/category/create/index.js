import { useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import {
  Container, SubContainer, CreateHeader, CreateHeaderUL,
  GridContainer, Colum1, Colum2, CardFrom, CreateHeaderTitle, NavLink,
  Form, Input, Label, LabelFile, Description, CloudIcon, CloseIconFile,
  FileContainer, FileContent, FormFile, FileWrapper, PreviewImageDiv, CustomBTN,
  SLabel, SubmitContainer, SubmitButton
} from '~/view/admin/category/create/styled'
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { create as createCategory } from '~/store/category/category.action'

const CategoryCreate = (props) => {
  const dispatch = useDispatch()

  const [form, setForm] = useState({
    status: false
  })

  const [preview, setPreview] = useState('')

  const loading = useSelector((state) => state.category.loading)

  const handlerChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleSwitch = () => setForm({ ...form, status: !form.status })

  const custonBtn = () => {
    document.getElementById('dafault-btn').click()
  }

  const previewImage = (props) => {
    const image = props.target.files[0]
    const url = URL.createObjectURL(image)
    setPreview(url)
    setForm({
      ...form,
      image
    })
  }

  const removeImage = () => {
    delete form.image
    setForm(form)
    setPreview('')
  }

  const cleanup = useCallback(() => {
    setTimeout(() => {
      setForm({ status: false })
      setPreview('')
    }, 2000)
  }, [])

  const HandleSubmit = () => {
    dispatch(createCategory(form))
    cleanup()
  }

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Create a new category </CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li><NavLink to="/admin/category">Category</NavLink></li>
            <li aria-disabled>Create</li>
          </CreateHeaderUL>
        </CreateHeader>
        <GridContainer>
          <Colum1>
            <CardFrom>
              <Form>
                <Input type="text" name="name" id="email" className="form--input" autoComplete="off" value={form.name || ''} onChange={handlerChange} required />
                <Label htmlFor="name" className="form--label">Category Name</Label>
              </Form>
              <FormFile>
                <SLabel htmlFor="description">Descrição</SLabel>
                <Description type="text" name="description" id="description" value={form.description || ''} onChange={handlerChange} />
              </FormFile>
              <FormFile>
                <FileContainer>
                  <FileWrapper>
                    <PreviewImageDiv className="image">
                      {preview.length > 0 ? <img src={preview} /> : ''}
                    </PreviewImageDiv>
                    <FileContent>
                      <div className="icon"><CloudIcon /></div>
                      <div className="text">No file chosen, yet!</div>
                    </FileContent>
                    <div className={preview ? 'cancel-btn' : 'cancel-btn active'} onClick={removeImage}><CloseIconFile /></div>
                    {/* <div className="file-name" id="file-name">File name here</div> */}
                  </FileWrapper>
                </FileContainer>
                <LabelFile htmlFor="file-upload">Add Image</LabelFile>
                <input id="dafault-btn" type="file" accept="image/*" name="image" onChange={previewImage} hidden />
                <CustomBTN name="file-upload" onClick={custonBtn}> Choose a file</CustomBTN>
              </FormFile>
            </CardFrom>
          </Colum1>
          <Colum2>
            <CardFrom>
              <FormControlLabel
                control={<Switch checked={form.status} onChange={handleSwitch} name="status" color="secondary" />}
                label="Status"
              />
            </CardFrom>
            <SubmitContainer>
              <SubmitButton id="custom-btn" type="submit" onClick={HandleSubmit}>Criar Categoria</SubmitButton>
            </SubmitContainer>
          </Colum2>
        </GridContainer>
      </SubContainer>

    </Container>
  )
}

export default CategoryCreate
