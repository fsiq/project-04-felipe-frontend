import styled from 'styled-components'
import { Link } from '@reach/router'

import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import CloseIcon from '@material-ui/icons/Close'

// ICONS

const CloudIcon = styled(CloudUploadIcon)`
  font-size: 100px;
  color: #5ac997;
`

const CloseIconFile = styled(CloseIcon)`
  font-size: 20px;
  color: green;
`

// Body
const Container = styled.div`
  padding: 40px 80px;
  height: 100%;
  @media only screen and (max-width: 978px) {
    padding: 0;
  }
`

const SubContainer = styled.div`
  padding-left: 24px;
  padding-right: 24px;

  @media only screen and (max-width: 1280px) {
    padding: 30px;
  }
`

const CreateHeader = styled.div`
  display: flex;
  flex-direction: column;
`

const CreateHeaderTitle = styled.h2`
  font-size: 1.5rem;
`

const CreateHeaderUL = styled.ul`
  display: flex;
  padding-top: 10px;
  padding-bottom: 35px;
  margin-left: 25px;
  
  li {
    font-size: 15px;
    font-style: italic;
    margin-left: 30px;
    color: #a3adb8;
    
    &:first-child {
      margin-left: 0;
      list-style: none;
    }
  }
  span {
    color: black;
  }

`

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.4fr;
  grid-template-rows: none;
  grid-template-areas: 
  'colum1 colum2';
  @media only screen and (max-width: 978px) {
    display: flex;
    flex-direction: column;
  }
`

const Colum1 = styled.div`
  grid-area: colum1;
  height: 100%;
`

const Colum2 = styled.div`
  grid-area: colum2;
  margin-left: 20px;
  height: 100%;
  margin-bottom: 20px;
  @media only screen and (max-width: 978px) {
    margin-left: 0
  }
`

const CardFrom = styled.div`
  background-color: #fff;
  color: rgb(33, 43, 54);
  box-shadow: rgb(145 158 171 / 24%) 0px 0px 2px 0px, rgb(145 158 171 / 24%) 0px 16px 32px -4px;
  border-radius: 16px;
  padding: 24px;
  margin-bottom: 15px;
`

// LINK
const NavLink = styled(Link)`
  text-decoration: none;
  font-style: italic;
  color: black;
  &:hover {
    text-decoration: underline;
  }
`

// FORM
const Form = styled.div``

const FormFile = styled.div`
  margin-bottom: 25px;
`

const Input = styled.input`
  width: 100%;
  height: 100%;
  font-family:inherit;
  font-size: inherit;
  padding: 1.15rem;
  background: none;
  border: 1px solid rgba(163, 173, 184, 0.3);
  border-radius: 0.5rem;
  transition: 200ms ease-in;
  &:hover{
    border-color: #000;
  }
  &:focus {
    border: 2px solid;
    border-color: green;
  }
`

const Description = styled.textarea`
  width: 100%;
  min-height: 250px;
  resize: none;
  font-family:inherit;
  font-size: inherit;
  padding: 1.15rem;
  background: none;
  border: 1px solid rgba(163, 173, 184, 0.3);
  border-radius: 0.5rem;
`

const FileContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  height: 200px;
  width: 100%;
  padding: 40px 8px;
  background-color: rgb(244, 246, 248);
  border: 1px dashed rgba(145, 158, 171, 0.32);
  border-radius: 8px;
`

const FileWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  /* .file-name {
    position: absolute;
    bottom: 0;
    background: linear-gradient(135deg, #5ac997 20%,#3ea175 100%);
    border-radius: 0 0 8px 8px;
    width: 100%;
    text-align: center;
    font-size: 18px;
    color: #fff;
    padding: 6px 0;
    display: none;
  } */
  .cancel-btn {
    position: absolute;
    right: 15px;
    top: 15px;
    cursor: pointer;
  }
  .cancel-btn.active{
    display: none;
  }
`

const PreviewImageDiv = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    height: 100%;
    width: 100%;
    object-fit: cover;
    border: none;
    border-radius: 8px;
  }
`

const FileContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  .text {
    font-size: 20px;
    font-weight: 500;
    margin-left: 20px;
  }
`

const CustomBTN = styled.button`
  width: 100%;
  height: 40px;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  letter-spacing: 1px;
  cursor: pointer;
  color: #fff;
  border: none;
  border-radius: 25px;
  background: linear-gradient(135deg, #5ac997 20% ,#3ea175 100%);
  position: relative;
  bottom: -20px;
`

const LabelFile = styled.label`
    font-weight: 600;
    font-size: 0.875rem;
    line-height: 1.57143;
    color: rgb(99, 115, 129);
    position: absolute;
    top: 575px;
    @media only screen and (max-width: 978px) {
      position: absolute;
      top: 565px;
    }
    
`

const Label = styled.label`
  position: relative;
  color: rgba(163, 173, 184, 0.8);
  top: -38px;
  right: -1rem;
  :after {
    background-color:black
  }
`

const SLabel = styled.label`
  font-weight: 600;
  font-size: 0.875rem;
  line-height: 1.57143;
  color: rgb(99, 115, 129);
`
const SubmitContainer = styled.div`
  width: 100%;
`
const SubmitButton = styled.button`
  box-sizing: border-box;
  -webkit-tap-highlight-color: transparent;
  cursor: pointer;
  user-select: none;
  vertical-align: middle;
  appearance: none;
  text-decoration: none;
  font-weight: 700;
  font-size: 0.9375rem;
  line-height: 1.71429;
  text-transform: capitalize;
  min-width: 64px;
  padding: 8px 22px;
  border-radius: 8px;
  transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  color: rgb(255, 255, 255);
  background-color: rgb(0, 171, 85);
  width: 100%;
  box-shadow: rgb(0 171 85 / 24%) 0px 8px 16px 0px;
  height: 48px;
  border: none;
  &:hover {
    background-color: #007b55;
  }
`

//
export {
  Form,
  FormFile,
  Label,
  SLabel,
  Input,
  NavLink,
  Container,
  SubContainer,
  CreateHeader,
  CreateHeaderUL,
  GridContainer,
  Colum1,
  Colum2,
  CardFrom,
  CreateHeaderTitle,
  Description,
  CloudIcon,
  CloseIconFile,
  FileContainer,
  FileContent,
  FileWrapper,
  PreviewImageDiv,
  LabelFile,
  CustomBTN,
  SubmitContainer,
  SubmitButton
}
