import { useState } from 'react'
import { useDispatch } from 'react-redux'
// useSelector,useCallback, useEffect
import {
  Container, SubContainer, CreateHeader, CreateHeaderUL,
  GridContainer, Colum1, Colum2, CardFrom, CreateHeaderTitle, NavLink,
  Form, Input, Label, LabelFile, Description, CloudIcon, CloseIconFile,
  FileContainer, FileContent, FormFile, FileWrapper, PreviewImageDiv, CustomBTN,
  SLabel, SubmitContainer, SubmitButton
} from '~/view/admin/category/create/styled'
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { update as updateCategory } from '~/store/category/category.action'

const CategoryEdit = (props) => {
  const dispatch = useDispatch()

  const { id, name, description, status, image } = props?.location.state.category
  const [form, setForm] = useState({
    name,
    description,
    status
  })

  const [preview, setPreview] = useState(process.env.REACT_APP_STATIC + image)

  const handlerChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleSwitch = () => setForm({ ...form, status: !form.status })

  const custonBtn = () => {
    document.getElementById('dafault-btn').click()
  }

  const previewImage = (props) => {
    const image = props.target.files[0]
    const url = URL.createObjectURL(image)
    setPreview(url)
    setForm({
      ...form,
      image
    })
  }

  const removeImage = () => {
    delete form.image
    setForm(form)
    setPreview('')
  }

  const HandleSubmit = () => {
    dispatch(updateCategory(id, form))
  }

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Create a new category </CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li><NavLink to="/admin/category">Category</NavLink></li>
            <li aria-disabled>{props.categoryName}</li>
          </CreateHeaderUL>
        </CreateHeader>
        <GridContainer>
          <Colum1>
            <CardFrom>
              <Form>
                <Input type="text" name="name" id="email" className="form--input" autoComplete="off" value={form.name || ''} onChange={handlerChange} required />
                <Label htmlFor="name" className="form--label">Category Name</Label>
              </Form>
              <FormFile>
                <SLabel htmlFor="description">Descrição</SLabel>
                <Description type="text" name="description" id="description" value={form.description || ''} onChange={handlerChange} />
              </FormFile>
              <FormFile>
                <FileContainer>
                  <FileWrapper>
                    <PreviewImageDiv className="image">
                      {preview.length > 0 ? <img src={preview} /> : ''}
                    </PreviewImageDiv>
                    <FileContent>
                      <div className="icon"><CloudIcon /></div>
                      <div className="text">No file chosen, yet!</div>
                    </FileContent>
                    <div className={preview ? 'cancel-btn' : 'cancel-btn active'} onClick={removeImage}><CloseIconFile /></div>
                  </FileWrapper>
                </FileContainer>
                <LabelFile htmlFor="file-upload">Add Image</LabelFile>
                <input id="dafault-btn" type="file" accept="image/*" name="image" onChange={previewImage} hidden />
                <CustomBTN name="file-upload" onClick={custonBtn}> Choose a file</CustomBTN>
              </FormFile>
            </CardFrom>
          </Colum1>
          <Colum2>
            <CardFrom>
              <FormControlLabel
                control={<Switch checked={form.status} onChange={handleSwitch} name="status" color="secondary" />}
                label="Status"
              />
            </CardFrom>
            <SubmitContainer>
              <SubmitButton id="custom-btn" type="submit" onClick={HandleSubmit}>Atualizar Categoria</SubmitButton>
            </SubmitContainer>
          </Colum2>
        </GridContainer>
      </SubContainer>

    </Container>
  )
}

export default CategoryEdit
