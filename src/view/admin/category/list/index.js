import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Container, SubContainer, CreateHeader, CreateHeaderTitle, CreateHeaderUL, NavLink, OtherStyledLink, CustomButtonHeader, Add } from '~/view/admin/category/list/styled'
import DataList from '~/components/layout/category/datagrid'
import { Grid, CssBaseline } from '@material-ui/core'
import { CardFrom } from '~/view/admin/category/create/styled'
import { getAll as getCategories } from '~/store/category/category.action'

const CategoryList = () => {
  const dispatch = useDispatch()

  const categories = useSelector((state) => state.category.all)
  const loading = useSelector((state) => state.category.loading)

  const callCategory = useCallback(() => {
    dispatch(getCategories())
  }, [dispatch])

  useEffect(() => {
    callCategory()
  }, [callCategory])

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Create a new category </CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li><NavLink to="/admin/category">Category</NavLink></li>
            <li aria-disabled>List</li>
          </CreateHeaderUL>
          <CustomButtonHeader>
            <Add />
            <OtherStyledLink to="/admin/category/create">Nova Categoria</OtherStyledLink>
          </CustomButtonHeader>
        </CreateHeader>
        <Grid>
          <CssBaseline />
          <Grid item md={12} xl={8} />
          <CardFrom>
            <DataList data={categories} loading={loading} />
          </CardFrom>
        </Grid>

      </SubContainer>
    </Container>
  )
}

export default CategoryList
