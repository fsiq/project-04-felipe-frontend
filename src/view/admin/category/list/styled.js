import styled from 'styled-components'
import { Link } from '@reach/router'

import AddIcon from '@material-ui/icons/Add'

// icons
const Add = styled(AddIcon)`
  font-size: 16px;
  position: relative;
  right: 20px;
  color: #fff;
`

const Container = styled.div`
  padding: 40px 80px;
  height: 100%;
  @media only screen and (max-width: 978px) {
    padding: 0;
  }
`
const SubContainer = styled.div`
  padding-left: 24px;
  padding-right: 24px;

  @media only screen and (max-width: 1280px) {
    padding: 30px;
  }
`
const CreateHeader = styled.div`
  display: flex;
  flex-direction: column;
`

const CreateHeaderTitle = styled.h2`
  font-size: 1.5rem;
`

const CreateHeaderUL = styled.ul`
  display: flex;
  padding-top: 10px;
  padding-bottom: 35px;
  margin-left: 25px;
  
  li {
    font-size: 15px;
    font-style: italic;
    margin-left: 30px;
    color: #a3adb8;
    
    &:first-child {
      margin-left: 0;
      list-style: none;
    }
  }
  span {
    color: black;
  }

`
// LINK
const NavLink = styled(Link)`
  text-decoration: none;
  font-style: italic;
  color: black;
  &:hover {
    text-decoration: underline;
  }
`
const OtherStyledLink = styled(Link)`
  text-decoration: none;
  color: #fff;
`
const CustomButtonHeader = styled.div`
    width: 200px;
    align-items: center;
    display: inline-flex;
    justify-content: center;
    position: absolute;
    top: 110px;
    right: 250px;
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
    outline: 0px;
    border: 0px;
    margin: 0px;
    cursor: pointer;
    user-select: none;
    vertical-align: middle;
    appearance: none;
    text-decoration: none;
    font-weight: 700;
    font-size: 0.875rem;
    line-height: 1.71429;
    text-transform: capitalize;
    padding: 6px 16px;
    border-radius: 8px;
    transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    color: rgb(255, 255, 255);
    background-color: rgb(0, 171, 85);
    box-shadow: rgb(0 171 85 / 24%) 0px 8px 16px 0px;
    @media only screen and (max-width: 978px) {
      top: 160px;
      right: 30px;
    }
    ${(props) => props.calendar && `
      right: 60px;
    `}
`
const Card = styled.div`
  background-color: #fff;
  color: rgb(33, 43, 54);
  box-shadow: rgb(145 158 171 / 24%) 0px 0px 2px 0px, rgb(145 158 171 / 24%) 0px 16px 32px -4px;
  border-radius: 16px;
  padding: 24px;
  margin-bottom: 15px;
`
export {
  Container,
  SubContainer,
  CreateHeader,
  CreateHeaderTitle,
  CreateHeaderUL,
  NavLink,
  OtherStyledLink,
  CustomButtonHeader,
  Add,
  Card
}
