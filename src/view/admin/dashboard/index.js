import { useState } from 'react'

import styled from 'styled-components'

import SideBar from '~/components/layout/dashboard/sidebar'
import NavBar from '~/components/layout/dashboard/navbar'

const GridContainer = styled.div`
  display: grid;
  height: 100vh;
  grid-template-columns: 0.4fr 3fr;
  grid-template-rows: 0.2fr 3fr;
  grid-template-areas: 
    'sidebar nav nav nav'
    'sidebar main main main';
    @media only screen and (max-width: 978px) {
      grid-template-columns: 1fr;
      grid-template-rows: 0.2fr 3fr;
      grid-template-areas: "nav" "main";
    }
`
const DashBoard = (props) => {
  const [sideBar, setOpenSideBar] = useState(false)

  const toggleMenu = () => {
    setOpenSideBar(!sideBar)
  }

  return (
    <GridContainer>
      <SideBar sideBar={sideBar} toggle={toggleMenu} location={location} />
      {props.children}
      <NavBar sideBar={sideBar} toggle={toggleMenu} />
    </GridContainer>
  )
}

export default DashBoard
