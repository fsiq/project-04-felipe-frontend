import { Router } from '@reach/router'
import { useSelector } from 'react-redux'

import Dashboard from '~/view/admin/dashboard'

// views
import CategoryList from '~/view/admin/category/list'
import CategoryCreate from '~/view/admin/category/create'
import CategoryEdit from '~/view/admin/category/edit'
import Users from '~/view/admin/users/index'
import UsersEdit from '~/view/admin/users/edit/index'
import Paterns from '~/view/admin/partners'
import PaternsEdit from '~/view/admin/partners/edit'
import Main from '~/view/admin/app'
import Calendar from '~/view/admin/calendar'
import Settings from '~/view/admin/profile-edit'

const Menu = [
  {
    title: 'App',
    route: '/',
    visibleMenu: true,
    enabled: true,
    component: Main,
    authorization: [1]
  },
  {
    title: 'Category Create',
    route: '/category/create',
    visibleMenu: true,
    enabled: true,
    component: CategoryCreate,
    authorization: [1]
  },
  {
    title: 'Category Edit',
    route: '/category/:categoryName/edit',
    visibleMenu: true,
    enabled: true,
    component: CategoryEdit,
    authorization: [1]
  },
  {
    title: 'Category',
    route: '/category',
    visibleMenu: true,
    enabled: true,
    component: CategoryList,
    authorization: [1]
  },
  {
    title: 'Users',
    route: '/users/list',
    visibleMenu: true,
    enabled: true,
    component: Users,
    authorization: [1]
  },
  {
    title: 'Users',
    route: '/users/:name/edit',
    visibleMenu: true,
    enabled: true,
    component: UsersEdit,
    authorization: [1]
  },
  {
    title: 'Paterns',
    route: '/paterns',
    visibleMenu: true,
    enabled: true,
    component: Paterns,
    authorization: [1]
  },
  {
    title: 'Paterns',
    route: '/paterns/:name/edit',
    visibleMenu: true,
    enabled: true,
    component: PaternsEdit,
    authorization: [1]
  },
  {
    title: 'Calendar',
    route: '/calendar',
    visibleMenu: true,
    enabled: true,
    component: Calendar,
    authorization: [1]
  },
  {
    title: 'Settings',
    route: '/settings',
    visibleMenu: true,
    enabled: true,
    component: Settings,
    authorization: [1]
  },
]

const Admin = (props) => {
  const kindUser = useSelector((state) => state.auth.usuario.kindUser)
  const autenticatedRoutes = Menu.filter((route) =>
    route.authorization.includes(kindUser)
  )
  const NotFound = () => <h2>Não autorizado</h2>

  return (
    <>
      <Router>
        <Dashboard path="/">
          {autenticatedRoutes.map(({ component: Component, route }, i) => (
            <Component key={i} path={route} />
          ))}
          <NotFound default />
        </Dashboard>
      </Router>
    </>
  )
}

export default Admin
