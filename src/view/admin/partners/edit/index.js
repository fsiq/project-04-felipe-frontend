import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { updatePartnerAction } from '~/store/partner/partner.action'
// import Switch from '@material-ui/core/Switch'
import {
  Container, SubContainer, CreateHeader, CreateHeaderUL, CreateHeaderTitle, NavLink,
  Input, Label,
} from '~/view/admin/category/create/styled'

import {
  GridContainer, Colum1, Colum2, CardFrom, Form, FormContainer,
  SubmitContainer, SubmitButton, ProfileImageContainer, Span,
  BorderProfileContainer, PhotoIcon, Test
} from './styled'

import Switch from '@material-ui/core/Switch'

import defaultImg from '~/assets/images/fallback-images/defaultUser.png'

const PaternsEdit = (props) => {
  const dispatch = useDispatch()

  const { id, name, email, accountable, address, cnpj, companyName, city, phone, status, uf, zip, image } = props?.location.state.partner

  const [preview, setPreview] = useState(process.env.REACT_APP_STATIC + image)

  const [form, setForm] = useState({
    name,
    email,
    accountable,
    uf,
    city,
    address,
    cnpj,
    companyName,
    id,
    zip,
    phone,
    status,
  })

  const handlerChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleSwitch = () => {
    if (form.status === 'Analise') {
      setForm({ ...form, status: 'Ativo' })
    }
    if (form.status === 'Ativo') {
      setForm({ ...form, status: 'Inativo' })
    } else {
      setForm({ ...form, status: 'Ativo' })
    }
  }

  const custonBtn = () => {
    document.getElementById('dafault-btn').click()
  }

  const previewImage = (props) => {
    const image = props.target.files[0]
    const url = URL.createObjectURL(image)
    setPreview(url)
    setForm({
      ...form,
      image
    })
  }

  const removeImage = () => {
    delete form.image
    setForm(form)
    setPreview('')
  }

  const HandleSubmit = () => {
    dispatch(updatePartnerAction(id, form))
  }

  const UpdatePreview = (preview === `${process.env.REACT_APP_STATIC}/static/profile/undefined`) ? setPreview(false) : preview

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Edit Partner</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li><NavLink to="/admin/paterns">Paterns</NavLink></li>
            <li aria-disabled>{props?.location.state.partner.companyName}</li>
          </CreateHeaderUL>
        </CreateHeader>
        <GridContainer>
          <Colum1>
            <CardFrom>
              <Span inative={form.status === 'Inativo'} isAnalise={form.status === 'Analise'}>{form.status}</Span>
              <ProfileImageContainer>
                <BorderProfileContainer>
                  <input id="dafault-btn" type="file" accept="image/*" name="image" onChange={previewImage} hidden />
                  <img src={UpdatePreview || defaultImg} />
                  <div className="update-div hover" onClick={custonBtn}>
                    <PhotoIcon />
                    <span>Update photo</span>
                  </div>
                </BorderProfileContainer>
                <p>Allowed *.jpeg, *.jpg, *.png, *.gif
                  max size of 3.1 MB
                </p>
              </ProfileImageContainer>
              <Test>
                <label name="status" className="test2">
                  <h6>Status</h6>
                  <span className="sub-title--perfil">Aplicar conta desativada</span>
                </label>
                <span><Switch checked={form.status === 'Ativo'} onChange={handleSwitch} name="status" color="secondary" /></span>
              </Test>
              <Test>
                <label name="status" className="test2">
                  <h6>Email Verified</h6>
                  <span className="sub-title--perfil">é apenas um mock de ativação de email</span>
                </label>
                <span><Switch checked={false} name="status" color="secondary" /></span>
              </Test>
            </CardFrom>
          </Colum1>
          <Colum2>
            <CardFrom>
              <Form>
                <FormContainer>
                  <Input type="text" name="name" id="name" className="form--input" autoComplete="off" value={form.name || ''} onChange={handlerChange} required />
                  <Label htmlFor="name" className="form--label">Nome Completo</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="email" id="email" className="form--input" autoComplete="off" value={form.email || ''} onChange={handlerChange} required />
                  <Label htmlFor="email" className="form--label">E-mail</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="companyName" id="companyName" className="form--input" autoComplete="off" value={form.companyName || ''} onChange={handlerChange} required />
                  <Label htmlFor="companyName" className="form--label">Company Name</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="phone" id="phone" className="form--input" autoComplete="off" value={form.phone || ''} onChange={handlerChange} required />
                  <Label htmlFor="phone" className="form--label">Telefone</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="cnpj" id="cnpj" className="form--input" autoComplete="off" value={form.cnpj || ''} onChange={handlerChange} required />
                  <Label htmlFor="cnpj" className="form--label">Cnpj</Label>
                </FormContainer>
                <FormContainer>
                  <Input name="uf" id="uf" className="form--input" autoComplete="off" value={form.uf || ''} onChange={handlerChange} required />
                  <Label htmlFor="uf" className="form--label">UF</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="accountable" id="accountable" className="form--input" autoComplete="off" value={form.accountable || ''} onChange={handlerChange} required />
                  <Label htmlFor="accountable" className="form--label">Accountable</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="city" id="city" className="form--input" autoComplete="off" value={form.city || ''} onChange={handlerChange} required />
                  <Label htmlFor="city" className="form--label">Cidade</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="address" id="address" className="form--input" autoComplete="off" value={form.address || ''} onChange={handlerChange} required />
                  <Label htmlFor="address" className="form--label">Endereço</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="zip" id="zip" className="form--input" autoComplete="off" value={form.zip || ''} onChange={handlerChange} required />
                  <Label htmlFor="zip" className="form--label">Cep/Zip</Label>
                </FormContainer>
              </Form>
              <SubmitContainer>
                <SubmitButton id="custom-btn" type="submit" onClick={HandleSubmit}>Save Changes</SubmitButton>
              </SubmitContainer>
            </CardFrom>
          </Colum2>
        </GridContainer>
      </SubContainer>
    </Container>
  )
}

export default PaternsEdit
