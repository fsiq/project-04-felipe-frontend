import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Container, SubContainer, CreateHeader, CreateHeaderTitle, CreateHeaderUL, NavLink } from '~/view/admin/category/list/styled'
import DataList from '~/components/layout/partner/datagrid'
import { Grid, CssBaseline } from '@material-ui/core'
import { CardFrom } from '~/view/admin/category/create/styled'
import { listAllPartner } from '~/store/partner/partner.action'

const Partner = () => {
  const dispatch = useDispatch()

  const allPartner = useSelector((state) => state.partner.all)
  const loading = useSelector((state) => state.partner.loading)

  const callPartner = useCallback(() => {
    dispatch(listAllPartner())
  }, [dispatch])

  useEffect(() => {
    callPartner()
  }, [callPartner])

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Paterns List</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li aria-disabled>Paterns List</li>
          </CreateHeaderUL>
        </CreateHeader>
        <Grid>
          <CssBaseline />
          <Grid item md={12} xl={8} />
          <CardFrom>
            <DataList data={allPartner} loading={loading} />
          </CardFrom>
        </Grid>
      </SubContainer>
    </Container>
  )
}

export default Partner
