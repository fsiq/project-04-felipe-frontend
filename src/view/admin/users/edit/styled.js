import styled from 'styled-components'

import AddAPhotoIcon from '@material-ui/icons/AddAPhoto'

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 0.40fr 1fr;
  grid-template-rows: none;
  grid-template-areas: 
  'colum1 colum2';
  @media only screen and (max-width: 978px) {
    display: flex;
    flex-direction: column;
  }
`

export const Colum1 = styled.div`
  grid-area: colum1;
  height: 100%;
`

export const Colum2 = styled.div`
  grid-area: colum2;
  margin-left: 20px;
  height: 100%;
  margin-bottom: 20px;
  @media only screen and (max-width: 978px) {
    margin-left: 0
  }
`

export const CardFrom = styled.div`
  background-color: #fff;
  color: rgb(33, 43, 54);
  box-shadow: rgb(145 158 171 / 24%) 0px 0px 2px 0px, rgb(145 158 171 / 24%) 0px 16px 32px -4px;
  border-radius: 16px;
  padding: 24px;
  margin-bottom: 15px;
`

export const Form = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: center;
`

export const FormContainer = styled.div`
  width: calc(50% - 15px);
  margin-right: 15px;
  margin-bottom: px;
  @media only screen and (max-width: 978px) {
    width: 100%;
    margin-right: 0;  
  }
`

export const SubmitContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  @media only screen and (max-width: 978px) {
    margin-top: 5px;
    margin-bottom: 15px;
  }
`

export const SubmitButton = styled.button`
  box-sizing: border-box;
  -webkit-tap-highlight-color: transparent;
  cursor: pointer;
  user-select: none;
  vertical-align: middle;
  appearance: none;
  text-decoration: none;
  font-weight: 700;
  font-size: 0.9375rem;
  line-height: 1.71429;
  text-transform: capitalize;
  min-width: 64px;
  padding: 8px 22px;
  border-radius: 8px;
  transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  color: rgb(255, 255, 255);
  background-color: rgb(0, 171, 85);
  box-shadow: rgb(0 171 85 / 24%) 0px 8px 16px 0px;
  height: 38px;
  border: none;
  &:hover {
    background-color: #007b55;
  }
  @media only screen and (max-width: 978px) {
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    top: 20px
  }
`

export const ProfileImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: 50px;
  img {
    width: 100%;
    height: 100%;
    border-radius: 50%;
  }
  p {
    color: inherit;
    color: #a3adb8;
    text-align: center;
    padding: 24px;
  }
`

export const BorderProfileContainer = styled.div`
  width: 144px;
  height: 144px;
  margin: auto;
  border-radius: 50%;
  padding: 8px;
  border: 1px dashed rgba(145, 158, 171, 0.32);
  .update-div {
    display: flex;
    width: 100%;
    height: 100%;
    position: relative;
    top: -130px;
    align-items: center;
    flex-direction: column;
    justify-content: center;
    transition: opacity 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    color: rgb(255, 255, 255);
    border-radius: 50%;
    background-color: rgba(22, 28, 36, 0.5);
    opacity: 0;
  }
  &:hover{
    .update-div.hover {
      opacity: 1;
    }
  }
`

export const Span = styled.span`
  height: 22px;
  min-width: 22px;
  line-height: 0;
  border-radius: 8px;
  cursor: default;
  align-items: center;
  white-space: nowrap;
  display: inline-flex;
  justify-content: center;
  padding: 0px 8px;
  color: rgb(34, 154, 22);
  font-size: 0.75rem;
  font-family: "Public Sans", sans-serif;
  background-color: rgba(84, 214, 44, 0.16);
  font-weight: 700;
  text-transform: uppercase;
  position: absolute;
  left: 705px;
  ${(props) => props.inative && `
    height: 22px;
    min-width: 22px;
    line-height: 0;
    border-radius: 8px;
    cursor: default;
    align-items: center;
    white-space: nowrap;
    display: inline-flex;
    justify-content: center;
    padding: 0px 8px;
    color: rgb(183, 33, 54);
    font-size: 0.75rem;
    font-family: "Public Sans", sans-serif;
    background-color: rgba(255, 72, 66, 0.16);
    font-weight: 700;
    text-transform: uppercase;
    position: absolute;
    left: 705px;
  `}
  ${(props) => props.isAnalise && `
    height: 22px;
    min-width: 22px;
    line-height: 0;
    border-radius: 8px;
    cursor: default;
    align-items: center;
    white-space: nowrap;
    display: inline-flex;
    justify-content: center;
    padding: 0px 8px;
    color: rgb(173, 173, 3);
    font-size: 0.75rem;
    font-family: "Public Sans", sans-serif;
    background-color: rgba(255, 255, 114, 0.16);
    font-weight: 700;
    text-transform: uppercase;
    position: absolute;
    left: 705px;
  `}
  @media only screen and (max-width: 978px) {
    left: 280px 
  }
`

export const PhotoIcon = styled(AddAPhotoIcon)`
  margin-bottom: 7px;
`

export const SwitchUser = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 15px;
  h6 {
    margin: 0px 0px 4px;
    font-family: "Public Sans", sans-serif;
    font-weight: 600;
    font-size: 0.875rem;
    line-height: 1.57143;
  }
  .sub-title--perfil {
    margin: 0px;
    font-family: "Public Sans", sans-serif;
    font-weight: 400;
    font-size: 0.875rem;
    line-height: 1.57143;
    color: rgb(99, 115, 129);
  }
  .test2 {
    display: flex;
    flex-direction: column;
  }
`
