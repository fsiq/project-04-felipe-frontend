import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Container, SubContainer, CreateHeader, CreateHeaderTitle, CreateHeaderUL, NavLink } from '~/view/admin/category/list/styled'
import DataList from '~/components/layout/user/datagrid'
import { Grid, CssBaseline } from '@material-ui/core'
import { CardFrom } from '~/view/admin/category/create/styled'
import { listAllUsers } from '~/store/users/user.action'

const Users = () => {
  const dispatch = useDispatch()

  const allUsers = useSelector((state) => state.users.all)
  const loading = useSelector((state) => state.users.loading)
  const selected = useSelector((state) => state.users.selected)

  const callUsers = useCallback(() => {
    dispatch(listAllUsers())
  }, [dispatch])

  useEffect(() => {
    callUsers()
  }, [callUsers])

  if (loading) {
    return (
      <h2>Carregando..</h2>
    )
  }
  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>User List</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/admin">Dashboard</NavLink></li>
            <li aria-disabled>List</li>
          </CreateHeaderUL>
        </CreateHeader>
        <Grid>
          <CssBaseline />
          <Grid item md={12} xl={8} />
          <CardFrom>
            <DataList data={allUsers} loading={loading} />
          </CardFrom>
        </Grid>

      </SubContainer>
    </Container>
  )
}

export default Users
