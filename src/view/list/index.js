import { Router } from '@reach/router'

import Layout from '~/components/list-employee'

import ListEmployee from '~/view/list/employee'

const Menu = [
  {
    icons: '',
    route: '/',
    visibleMenu: true,
    enabled: true,
    component: ListEmployee
  }
]

const List = (props) => {
  return (
    <>
      <Router>
        <Layout path="/">
          {Menu.map(({ component: Component, route }, i) => (
            <Component key={i} path={route} />
          ))}
        </Layout>
      </Router>
    </>
  )
}

export default List
