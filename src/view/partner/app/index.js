import styled from 'styled-components'
import AdminHeloImg from '~/assets/images/admin-settings-male.png'

const Main = () => {
  return (
    <StyledMain>
      <div className="main__container">
        <div className="main__title">
          <img src={AdminHeloImg} />
          <p>Bem vindo ao seu Painel</p>
        </div>
      </div>
    </StyledMain>
  )
}

export default Main

const StyledMain = styled.main`
  grid-area: main; 
  height: 100%;
  display: flex;
`
