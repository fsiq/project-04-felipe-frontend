import { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Select from 'react-select'

import { getAll as getCategories } from '~/store/category/category.action'
import { createNewEmployeeAction } from '~/store/partner/partner.action'
import defaultImg from '~/assets/images/fallback-images/defaultUser.png'

import {
  Container, SubContainer, CreateHeader, CreateHeaderUL, CreateHeaderTitle, NavLink, Input, Label
} from '~/view/admin/category/create/styled'

import { GridContainer, Colum1, Colum2, CardFrom } from './styled'

import { ProfileImageContainer, BorderProfileContainer, PhotoIcon, Form, FormContainer, SwitchUser, SubmitContainer, SubmitButton } from '~/view/admin/users/edit/styled'

import Switch from '@material-ui/core/Switch'

const EmployeeCreate = (props) => {
  const dispatch = useDispatch()
  const { partnerId } = props.location.state
  const categories = useSelector(state => state.category.all)

  const callCategory = useCallback(() => {
    dispatch(getCategories())
  }, [dispatch])

  useEffect(() => {
    callCategory()
  }, [callCategory])

  const options = categories.map((item) => {
    return { value: item.id, label: item.name }
  })

  const [preview, setPreview] = useState('')
  const [form, setForm] = useState({
    status: 'Ativo',
  })

  const handlerChange = (props) => {
    const { value, name } = props.target
    setForm({
      ...form,
      [name]: value
    })
  }

  const HandlerSelect = (item) => {
    setForm({
      ...form,
      categoryId: item.value
    })
  }

  const previewImage = (props) => {
    const image = props.target.files[0]
    const url = URL.createObjectURL(image)
    setPreview(url)
    setForm({
      ...form,
      image
    })
  }

  const custonBtn = () => {
    document.getElementById('dafault-btn').click()
  }

  const UpdatePreview = (preview === `${process.env.REACT_APP_STATIC}/static/profile/undefined`) ? setPreview(false) : preview

  const cleanup = useCallback(() => {
    setTimeout(() => {
      setForm({ status: 'Ativo' })
      setPreview('')
    }, 2000)
  }, [])

  const HandleSubmit = () => {
    dispatch(createNewEmployeeAction(partnerId, form))
    console.log(form)
    // cleanup()
  }

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Edit User</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/partner">Dashboard</NavLink></li>
            <li><NavLink to="/partner/employee">Employee</NavLink></li>
            <li aria-disabled>create</li>
          </CreateHeaderUL>
        </CreateHeader>
        <GridContainer>
          <Colum1>
            <CardFrom>
              {/* <Span inative={form.status === 'Inativo'} isAnalise={form.status === 'Analise'}>{form.status}</Span> */}
              <ProfileImageContainer>
                <BorderProfileContainer>
                  <input id="dafault-btn" type="file" accept="image/*" name="image" onChange={previewImage} hidden />
                  <img src={UpdatePreview || defaultImg} />
                  <div className="update-div hover" onClick={custonBtn}>
                    <PhotoIcon />
                    <span>Update photo</span>
                  </div>
                </BorderProfileContainer>
                <p>Allowed *.jpeg, *.jpg, *.png, *.gif
                  max size of 3.1 MB
                </p>
              </ProfileImageContainer>
              <SwitchUser>
                <label name="status" className="test2">
                  <h6>Email Verified</h6>
                  <span className="sub-title--perfil">é apenas um mock de ativação de email</span>
                </label>
                <span><Switch checked={false} name="status" color="secondary" /></span>
              </SwitchUser>
            </CardFrom>
          </Colum1>
          <Colum2>
            <CardFrom>
              <Form>
                <FormContainer>
                  <Input type="text" name="name" id="name" className="form--input" autoComplete="off" value={form.name || ''} onChange={handlerChange} required />
                  <Label htmlFor="name" className="form--label">Nome Completo</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="email" id="email" className="form--input" autoComplete="off" value={form.email || ''} onChange={handlerChange} required />
                  <Label htmlFor="email" className="form--label">E-mail</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="password" id="password" className="form--input" autoComplete="off" value={form.password || ''} onChange={handlerChange} required />
                  <Label htmlFor="email" className="form--label">Senha</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="phone" id="phone" className="form--input" autoComplete="off" value={form.phone || ''} onChange={handlerChange} required />
                  <Label htmlFor="phone" className="form--label">Telefone</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="state" id="state" className="form--input" autoComplete="off" value={form.state || ''} onChange={handlerChange} required />
                  <Label htmlFor="state" className="form--label">Estado</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="city" id="city" className="form--input" autoComplete="off" value={form.city || ''} onChange={handlerChange} required />
                  <Label htmlFor="city" className="form--label">Cidade</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="street" id="street" className="form--input" autoComplete="off" value={form.street || ''} onChange={handlerChange} required />
                  <Label htmlFor="street" className="form--label">Endereço</Label>
                </FormContainer>
                <FormContainer>
                  <Input type="text" name="zip" id="zip" className="form--input" autoComplete="off" value={form.zip || ''} onChange={handlerChange} required />
                  <Label htmlFor="zip" className="form--label">Cep/Zip</Label>
                </FormContainer>
                <FormContainer>
                  <Select options={options} onChange={HandlerSelect} placeholder="Categoria" />
                </FormContainer>
              </Form>
              <SubmitContainer>
                <SubmitButton id="custom-btn" type="submit" onClick={HandleSubmit}>Save Changes</SubmitButton>
              </SubmitContainer>
            </CardFrom>
          </Colum2>
        </GridContainer>
      </SubContainer>
    </Container>
  )
}

export default EmployeeCreate
