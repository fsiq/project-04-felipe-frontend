import styled from 'styled-components'

import Select from 'react-select'

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 0.40fr 1fr;
  grid-template-rows: none;
  grid-template-areas: 
  'colum1 colum2';
  @media only screen and (max-width: 978px) {
    display: flex;
    flex-direction: column;
  }
`
export const Colum1 = styled.div`
  grid-area: colum1;
  width: 100%;
  position: relative;
  height: 100%;
`

export const Colum2 = styled.div`
  grid-area: colum2;
  margin-left: 20px;
  height: 100%;
  margin-bottom: 20px;
  @media only screen and (max-width: 978px) {
    margin-left: 0
  }
`
export const CardFrom = styled.div`
  background-color: #fff;
  color: rgb(33, 43, 54);
  box-shadow: rgb(145 158 171 / 24%) 0px 0px 2px 0px, rgb(145 158 171 / 24%) 0px 16px 32px -4px;
  border-radius: 16px;
  padding: 24px;
  margin-bottom: 15px;
`
export const Span = styled.span`
  height: 22px;
  min-width: 22px;
  line-height: 0;
  border-radius: 8px;
  cursor: default;
  align-items: center;
  white-space: nowrap;
  display: inline-flex;
  justify-content: center;
  padding: 0px 8px;
  color: rgb(34, 154, 22);
  font-size: 0.75rem;
  font-family: "Public Sans", sans-serif;
  background-color: rgba(84, 214, 44, 0.16);
  font-weight: 700;
  text-transform: uppercase;
  position: relative;
  left: 705px;
  ${(props) => props.inative && `
    height: 22px;
    min-width: 22px;
    line-height: 0;
    border-radius: 8px;
    cursor: default;
    align-items: center;
    white-space: nowrap;
    display: inline-flex;
    justify-content: center;
    padding: 0px 8px;
    color: rgb(183, 33, 54);
    font-size: 0.75rem;
    font-family: "Public Sans", sans-serif;
    background-color: rgba(255, 72, 66, 0.16);
    font-weight: 700;
    text-transform: uppercase;
    position: absolute;
    left: 705px;
  `}
  ${(props) => props.isAnalise && `
    height: 22px;
    min-width: 22px;
    line-height: 0;
    border-radius: 8px;
    cursor: default;
    align-items: center;
    white-space: nowrap;
    display: inline-flex;
    justify-content: center;
    padding: 0px 8px;
    color: rgb(173, 173, 3);
    font-size: 0.75rem;
    font-family: "Public Sans", sans-serif;
    background-color: rgba(255, 255, 114, 0.16);
    font-weight: 700;
    text-transform: uppercase;
    position: absolute;
    left: 705px;
  `}
  @media only screen and (max-width: 978px) {
    left: 280px 
  }
`
export const StyledSelect = styled(Select)`
  width: 100%;
  height: 100%;
  font-family:inherit;
  font-size: inherit;
  background: none;
  border: 1px solid rgba(163, 173, 184, 0.3);
  border-radius: 0.5rem;
  transition: 200ms ease-in;
  &:hover{
    border-color: #000;
  }
  &:focus {
    border: 2px solid;
    border-color: green;
  }
`
