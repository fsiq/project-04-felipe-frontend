import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import DataList from '~/components/partner/employee/datagrid'
import { Container, SubContainer, CreateHeader, CreateHeaderTitle, CreateHeaderUL, NavLink, CustomButtonHeader, Add, OtherStyledLink } from '~/view/admin/category/list/styled'
import { Grid, CssBaseline } from '@material-ui/core'
import { CardFrom } from '~/view/admin/category/create/styled'
import { getEmployeeById } from '~/store/partner/partner.action'

const Employee = () => {
  const dispatch = useDispatch()

  const user = useSelector(state => state.auth.usuario)

  const employies = useSelector(state => state.partner?.employies)
  const loading = useSelector((state) => state.partner.loading)

  const { id } = user

  const callEmployee = useCallback(() => {
    dispatch(getEmployeeById(id))
  }, [dispatch, id])

  useEffect(() => {
    callEmployee()
  }, [callEmployee])

  return (
    <Container>
      <SubContainer>
        <CreateHeader>
          <CreateHeaderTitle>Employee List</CreateHeaderTitle>
          <CreateHeaderUL>
            <li><NavLink to="/partner">Dashboard</NavLink></li>
            <li aria-disabled>Employee List</li>
          </CreateHeaderUL>
          <CustomButtonHeader>
            <Add />
            <OtherStyledLink to="/partner/employee/create" state={{ partnerId: id }}>Novo Funcionário</OtherStyledLink>
          </CustomButtonHeader>
        </CreateHeader>
        <Grid>
          <CssBaseline />
          <Grid item md={12} xl={8} />
          <CardFrom>
            <DataList data={employies} loading={loading} />
          </CardFrom>
        </Grid>
      </SubContainer>
    </Container>
  )
}

export default Employee
