import { Router } from '@reach/router'
import { useSelector } from 'react-redux'

import Dashboard from '~/view/partner/dashboard'

import Main from '~/view/partner/app'
import Employee from '~/view/partner/employee'
import EmployeeCreate from '~/view/partner/employee/create'
import Settings from '~/view/partner/profile-edit'

const Menu = [
  {
    title: 'App',
    route: '/',
    visibleMenu: true,
    enabled: true,
    component: Main,
    authorization: [2]
  },
  {
    title: 'Employee',
    route: '/employee',
    visibleMenu: true,
    enabled: true,
    component: Employee,
    authorization: [2]
  },
  {
    title: 'Employee',
    route: '/employee/create',
    visibleMenu: true,
    enabled: true,
    component: EmployeeCreate,
    authorization: [2]
  },
  {
    title: 'Settings',
    route: '/settings',
    visibleMenu: true,
    enabled: true,
    component: Settings,
    authorization: [2]
  },
]

const Partner = (props) => {
  const kindUser = useSelector((state) => state.auth.usuario.kindUser)
  const autenticatedRoutes = Menu.filter((route) =>
    route.authorization.includes(kindUser)
  )
  const NotFound = () => <h2>Não autorizado</h2>

  return (
    <>
      <Router>
        <Dashboard path="/">
          {autenticatedRoutes.map(({ component: Component, route }, i) => (
            <Component key={i} path={route} />
          ))}
          <NotFound default />
        </Dashboard>
      </Router>
    </>
  )
}

export default Partner
