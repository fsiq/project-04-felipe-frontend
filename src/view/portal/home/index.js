import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Carousel from 'react-elastic-carousel'
import { listAllPartner } from '~/store/partner/partner.action'

import ComplexGrid from '~/components/portal-card/partner/partner-card'

import CategoryCard from '~/components/portal-card/category/category-card'
import { Container, Title, SubTitle, SearchContainer, SearchIcon, Input, Button, Label, CardCategoryContainer, Divisor, StyledCarousel } from '~/view/portal/home/styled'
import { Hr } from '~/components/layout/footer/styled'

import styled from 'styled-components'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}))

const Home = () => {
  const classes = useStyles()

  const dispatch = useDispatch()

  const callPartner = useCallback(() => {
    dispatch(listAllPartner())
  }, [dispatch])

  useEffect(() => {
    callPartner()
  }, [callPartner])

  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 },
  ]

  const partner = useSelector((state) => state.partner.all)

  return (
    <>
      <Container>
        <Title>Tudo pra facilitar seu dia a dia</Title>
        <SubTitle>oque você precisa está aqui</SubTitle>
        <SearchContainer>
          <SearchIcon />
          <Input type="text" name="search" id="search" className="search--input" placeholder="Endereço de busca e número" />
          <Label htmlFor="search" name="search" className="search--label">Endereço de busca e número</Label>
          <Button>Buscar</Button>
        </SearchContainer>
      </Container>
      <CardCategoryContainer>
        <CategoryCard />
      </CardCategoryContainer>
      <Divisor>
        <Hr />
        <h2>Os melhores espaços</h2>
        <StyledCarousel>
          <Carousel breakPoints={breakPoints} pagination={false}>
            {partner.map((item, k) => {
              return (
                <Card key={k}>
                  <ComplexGrid data={item} />
                </Card>
              )
            })}
          </Carousel>
        </StyledCarousel>
      </Divisor>
    </>
  )
}

export default Home

export const Card = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  width: 100%;
  margin: 25px 15px 0px 15px;
  background-color: #fff;
  border: 1px solid #f2f2f2;
  border-radius: 4px;
`
