import styled from 'styled-components'
import LocationSearchingIcon from '@material-ui/icons/LocationSearching'

const Container = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: rgba(247, 247 , 247, 1);
  @media only screen and (max-width: 978px) {
    padding: 0px 20px;
  }
`
const Title = styled.h2`
  
  font-size: 2rem;
  @media only screen and (max-width: 978px) {
    font-size: 1.25rem;
    margin-top: 20px;
  }
`
const SubTitle = styled.p`
  font-size: 0.75rem;
  margin-bottom: 40px;
  color: #717171;
  @media only screen and (max-width: 978px) {
    margin-bottom: 0px;
  }
`
// SEARCH BAR
const SearchContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-bottom: 40px;
  @media only screen and (max-width: 978px) {
    flex-direction: column;
    align-items: center;
  }
`
const SearchIcon = styled(LocationSearchingIcon)`
  width: 20px;
  height: 20px;
  position: relative;
  left: 30px;
  top: 10px;
  @media only screen and (max-width: 978px) {
    left: -150px;
    top: 30px
  }
`
const Label = styled.label`
  position: absolute;
  visibility: hidden;
`
const Input = styled.input`
  background-color: #ffffff;
  align-items: center;
  display: flex;
  justify-content: flex-start;
  width: 600px;
  padding: 12px 36px;
  border: 1px solid #f2f2f2;
  border-radius: 4px;
  color: rgba(71, 71, 71, 0.5);
  @media only screen and (max-width: 978px) {
    width: 100%;
    margin-left: 0px;
  }
  /* position: relative; */
`
const Button = styled.button`
  text-decoration: none;
  border: none;
  outline: none;
  border-radius: 4px;
  background-color: #171926;
  color: #fff;
  margin-left: 10px;
  right: 100px;
  padding: 12px 26px;
  @media only screen and (max-width: 978px) {
    width: 100%;
    margin-top: 15px;
  }
`

// CARD
const CardCategoryContainer = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Divisor = styled.div`
  padding: 50px 100px;
  @media only screen and (max-width: 978px) {
    padding: 50px 20px;
  }
`

export const StyledCarousel = styled.div`
  .rec.rec-arrow {
    width: 39px;
    height: 39px;
    background-color: #fff;
    color: black;
    border-radius: 50%;
    &.frcsNt {
      min-width: 39px;
      line-height: 39px;
      font-size: 1.2em;
    }
  }
  /* ocultar botões desativados */
  .rec.rec-arrow:disabled {
    visibility: hidden;
  }
  /* desativa o contorno padrão em itens focados  */
  /*  adicionar contorno personalizado em itens focados */
  .rec-carousel-item:focus {
    outline: none;
    box-shadow: inset 0 0 1px 1px lightgrey;
  }
`
export {
  Container,
  Title,
  SubTitle,
  SearchContainer,
  SearchIcon,
  Label,
  Input,
  Button,
  CardCategoryContainer
}
