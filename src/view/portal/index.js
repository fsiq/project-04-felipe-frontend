import { Router } from '@reach/router'

import Layout from '~/components/layout'
import Home from '~/view/portal/home/'

const Menu = [
  {
    title: 'Home',
    icons: '',
    route: '/',
    visibleMenu: true,
    enabled: true,
    component: Home
  }
]

const Portal = (props) => {
  return (
    <>
      <Router>
        <Layout path="/">
          {Menu.map(({ component: Component, route }, i) => (
            <Component key={i} path={route} />
          ))}
        </Layout>
      </Router>
    </>
  )
}

export default Portal
